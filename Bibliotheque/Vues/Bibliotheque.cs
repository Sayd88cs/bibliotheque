﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bibliotheque
{
    public partial class Bibliotheque : Form
    {
        Emprunt modelEmprunt = ModelReq.LoadModel<Emprunt>();

        //Variables utilisée dans le Form
        Dictionary<string, string> modelData = new Dictionary<string, string>();
        private string currentIdAdherent;
        private string currentIdEmprunteur;
        private string currentIdAuteur;
        private string currentIdEditeur;
        private string currentIdLivre;
        private string currentIdExemplaire;
        private string comboBoxIdAuteur;
        private string comboBoxIdEditeur;

        public Bibliotheque()
        {
            InitializeComponent();
            tabControlAdmin.Hide();
        }

        private void Bibliotheque_Load(object sender, EventArgs e)
        {
            rdHomme.Checked = true;
            RefreshTabAdherent();
            RefreshTabAuteur();
            RefreshTabEditeur();
            RefreshTabLivre();
            RefreshTabExemplaire();
            RefreshTabEmprunt();
        }

        private void toolStripAdministration_Click(object sender, EventArgs e)
        {
            tabControlAdmin.Show();
            tabControlGestion.Hide();
        }

        private void toolStripGestionEmprunts_Click(object sender, EventArgs e)
        {
            tabControlGestion.Show();
            tabControlAdmin.Hide();
            RefreshTabEmprunt();
        }

        private void dateTPickerAdherent_ValueChanged(object sender, EventArgs e)
        {
            txtDateNaiss.Text = dateTPickerAdherent.Value.ToShortDateString();
        }

        private void dateTPickerLivre_ValueChanged(object sender, EventArgs e)
        {
            txtDateEdition.Text = dateTPickerLivre.Value.ToShortDateString();
        }

        private void dateTPickerExemplaire_ValueChanged(object sender, EventArgs e)
        {
            txtDateAcquis.Text = dateTPickerExemplaire.Value.ToShortDateString();
        }

        /// <summary>                                                                           <summary>
        /// ------------------------------- PARTIE TAB ADHERENT --------------------------------------///
        /// </summary>                                                                          <summary>

        private void RefreshTabAdherent()
        {
            grdAdherent.Items.Clear();
            ListViewItem item;
            Adherent modelAdherent = ModelReq.LoadModel<Adherent>();
            foreach (Adherent unAdherent in modelAdherent.BDD_Read<Adherent>())
            {
                item = new ListViewItem(unAdherent.id);
                item.SubItems.Add(unAdherent.nom);
                item.SubItems.Add(unAdherent.prenom);
                item.SubItems.Add(unAdherent.sexe);
                string date = String.Format("{0:dd/MM/yyyy}", unAdherent.dateNaissance);
                item.SubItems.Add(date);
                item.SubItems.Add(unAdherent.adresse);
                item.SubItems.Add(unAdherent.ville);
                item.SubItems.Add(unAdherent.codePostal);
                grdAdherent.Items.Add(item);

                //Permet d'alterner la couleur de la ligne
                item.BackColor = item.Index % 2 == 0 ? Color.Chartreuse : Color.LightSkyBlue;
            }
        }

        /// <summary>
        /// Evenement qui se déclange au changement dans la ListView et qui permet de récuperer les données
        /// de la ligne selectionnée, ajoute aussi l'id a la propriété currentId.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdAdherent_SelectedIndexChanged(object sender, EventArgs e)
        {

            for (int i = 0; i < grdAdherent.Items.Count; i++)
                if (grdAdherent.Items[i].Selected == true)
                {
                    currentIdAdherent = grdAdherent.Items[i].SubItems[0].Text;
                    txtNom.Text = grdAdherent.Items[i].SubItems[1].Text;

                    //Verifie et assigne le bon sigle selon que soit coché Homme ou Femme
                    if (grdAdherent.Items[i].SubItems[3].Text == "F")
                    {
                        rdFemme.Checked = true;
                    }
                    else
                    {
                        rdHomme.Checked = true;
                    }

                    txtPrenom.Text = grdAdherent.Items[i].SubItems[2].Text;
                    txtAdresse.Text = grdAdherent.Items[i].SubItems[5].Text;
                    txtVille.Text = grdAdherent.Items[i].SubItems[6].Text;
                    txtDateNaiss.Text = grdAdherent.Items[i].SubItems[4].Text;
                    dateTPickerAdherent.Value = Convert.ToDateTime(txtDateNaiss.Text);
                    txtCP.Text = grdAdherent.Items[i].SubItems[7].Text;
                }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if ((txtAdresse.Text != string.Empty) && (txtCP.Text != string.Empty) && (txtNom.Text != string.Empty) && (txtPrenom.Text != string.Empty) && (txtVille.Text != string.Empty) && (txtDateNaiss.Text != string.Empty))
            {
                try
                {
                    string sexe = (rdHomme.Checked == true) ? "M" : "F";
                    Adherent unAdherent = new Adherent(txtNom.Text, txtPrenom.Text, sexe, Convert.ToDateTime(txtDateNaiss.Text), txtAdresse.Text, txtVille.Text, txtCP.Text);
                    AdherentControlleur Acontroll = new AdherentControlleur();
                    Acontroll.AjouterAdherent(unAdherent);
                    //clearer les input 
                    RefreshTabAdherent();
                }
                catch
                {
                    MessageBox.Show("Erreur champs mal renseignés");
                }
            }
            else
            {
                MessageBox.Show("Les champs ne sont pas tous complétés");
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if ((txtAdresse.Text != string.Empty) && (txtCP.Text != string.Empty) && txtNom.Text != string.Empty && txtPrenom.Text != string.Empty && txtVille.Text != string.Empty && txtDateNaiss.Text != string.Empty)
            {
                try
                {
                    string sexe = (rdHomme.Checked == true) ? "M" : "F";
                    Adherent unAdherent = new Adherent(txtNom.Text, txtPrenom.Text, sexe, Convert.ToDateTime(txtDateNaiss.Text), txtAdresse.Text, txtVille.Text, txtCP.Text, currentIdAdherent);
                    AdherentControlleur Acontroll = new AdherentControlleur();
                    Acontroll.AjouterAdherent(unAdherent);
                    RefreshTabAdherent();
                }
                catch
                {
                    MessageBox.Show("Erreur champs mal renseignés");
                }
            }
            else
            {
                MessageBox.Show("Les champs ne sont pas tous complétés");
            }
            RefreshTabAdherent();
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            Adherent modelAdherent = ModelReq.LoadModel<Adherent>();
            if (MessageBox.Show("Voulez vous vraiment suprimer cet adhérent ?", "Confirmation de supression", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (currentIdAdherent != null)
                {
                    modelAdherent.BDD_Delete(int.Parse(currentIdAdherent));
                    currentIdAdherent = null;
                }
                else
                {
                    MessageBox.Show("Vous devez selectionner un Adherent", "Information");
                }
                RefreshTabAdherent();
            }

        }

        /// <summary>                                                                           <summary>
        /// --------------------------- FIN PARTIE TAB ADHERENT --------------------------------------///
        /// ------------------------- DEBUT PARTIE TAB AUTEUR/EDITEUR --------------------------------///
        /// </summary>                                                                          <summary>


        /// --------------------------- Debut sous partie Auteur -------------------------------------///

        private void RefreshTabAuteur()
        {
            grdAuteur.Items.Clear();
            ListViewItem item;
            Auteur modelAuteur = ModelReq.LoadModel<Auteur>();
            foreach (Auteur unAuteur in modelAuteur.BDD_Read<Auteur>())
            {
                item = new ListViewItem(unAuteur.id);
                item.SubItems.Add(unAuteur.nom);
                item.SubItems.Add(unAuteur.prenom);
                grdAuteur.Items.Add(item);
                //Permet d'alterner la couleur de la ligne
                item.BackColor = item.Index % 2 == 0 ? Color.Chartreuse : Color.LightSkyBlue;
            }
        }

        private void grdAuteur_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < grdAuteur.Items.Count; i++)
                if (grdAuteur.Items[i].Selected == true)
                {
                    currentIdAuteur = grdAuteur.Items[i].SubItems[0].Text;
                    txtNomAuteur.Text = grdAuteur.Items[i].SubItems[1].Text;
                    txtPrenomAuteur.Text = grdAuteur.Items[i].SubItems[2].Text;
                }
        }

        private void btnAddAuteur_Click(object sender, EventArgs e)
        {
            if ((txtNomAuteur.Text != string.Empty) && (txtPrenomAuteur.Text != string.Empty))
            {
                try
                {
                    Auteur unAuteur = new Auteur(txtNomAuteur.Text, txtPrenomAuteur.Text);
                    AuteurControlleur Econtroll = new AuteurControlleur();
                    Econtroll.AjouterAuteur(unAuteur);
                    RefreshTabAuteur();
                    RefreshTabLivre();
                }
                catch
                {
                    MessageBox.Show("Erreur champs mal renseignés");
                }
            }
            else
            {
                MessageBox.Show("Les champs ne sont pas tous complétés");
            }
        }


        private void btnEditAuteur_Click(object sender, EventArgs e)
        {
            if ((txtNomAuteur.Text != string.Empty) && (txtPrenomAuteur.Text != string.Empty))
            {
                try
                {
                    Auteur unAuteur = new Auteur(txtNomAuteur.Text, txtPrenomAuteur.Text, currentIdAuteur);
                    AuteurControlleur Acontroll = new AuteurControlleur();
                    Acontroll.AjouterAuteur(unAuteur);
                    RefreshTabAuteur();
                    RefreshTabLivre();
                }
                catch
                {
                    MessageBox.Show("Erreur champs mal renseignés");
                }
            }
            else
            {
                MessageBox.Show("Les champs ne sont pas tous complétés");
            }
        }

        private void btnDelAuteur_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Voulez vous vraiment suprimer cet auteur ?\nCela suprimera aussi les livres associés!", "Confirmation de supression", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (currentIdAuteur != null)
                {
                    Auteur modelAuteur = ModelReq.LoadModel<Auteur>();
                    modelAuteur.BDD_Delete(int.Parse(currentIdAuteur));
                    currentIdAuteur = null;
                }
                else
                {
                    MessageBox.Show("Vous devez selectionner un Auteur", "Information");
                }
                RefreshTabAuteur();
                RefreshTabLivre();
                RefreshTabExemplaire();
            }
        }

        /// --------------------------- Debut sous partie Editeur ------------------------------------///

        private void RefreshTabEditeur()
        {

            grdEditeur.Items.Clear();
            ListViewItem item;
            Editeur modelEditeur = ModelReq.LoadModel<Editeur>();
            foreach (Editeur unEditeur in modelEditeur.BDD_Read<Editeur>())
            {
                item = new ListViewItem(unEditeur.id);
                item.SubItems.Add(unEditeur.nom);
                item.SubItems.Add(unEditeur.prenom);
                grdEditeur.Items.Add(item);
                //Permet d'alterner la couleur de la ligne
                item.BackColor = item.Index % 2 == 0 ? Color.Chartreuse : Color.LightSkyBlue;
            }
        }

        private void grdEditeur_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < grdEditeur.Items.Count; i++)
                if (grdEditeur.Items[i].Selected == true)
                {
                    currentIdEditeur = grdEditeur.Items[i].SubItems[0].Text;
                    txtNomEditeur.Text = grdEditeur.Items[i].SubItems[1].Text;
                    txtPrenomEditeur.Text = grdEditeur.Items[i].SubItems[2].Text;
                }
        }

        private void btnAddEditeur_Click(object sender, EventArgs e)
        {
            if ((txtNomEditeur.Text != string.Empty) && (txtPrenomEditeur.Text != string.Empty))
            {
                try
                {
                    Editeur unEditeur = new Editeur(txtNomEditeur.Text, txtPrenomEditeur.Text);
                    EditeurControlleur Econtroll = new EditeurControlleur();
                    Econtroll.AjouterEditeur(unEditeur);
                    RefreshTabEditeur();
                    RefreshTabLivre();
                }
                catch
                {
                    MessageBox.Show("Erreur champs mal renseignés");
                }
            }
            else
            {
                MessageBox.Show("Les champs ne sont pas tous complétés");
            }
        }

        private void btnEditEditeur_Click(object sender, EventArgs e)
        {
            if ((txtNomEditeur.Text != string.Empty) && (txtPrenomEditeur.Text != string.Empty))
            {
                try
                {
                    Editeur unEditeur = new Editeur(txtNomEditeur.Text, txtPrenomEditeur.Text, currentIdEditeur);
                    EditeurControlleur Econtroll = new EditeurControlleur();
                    Econtroll.AjouterEditeur(unEditeur);
                    RefreshTabEditeur();
                    RefreshTabLivre();
                }
                catch
                {
                    MessageBox.Show("Erreur champs mal renseignés");
                }
            }
            else
            {
                MessageBox.Show("Les champs ne sont pas tous complétés");
            }
        }

        private void btnDelEditeur_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Voulez vous vraiment suprimer cet editeur ?\nCela suprimera aussi les livres associés!", "Confirmation de supression", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (currentIdEditeur != null)
                {
                    Editeur modelEditeur = ModelReq.LoadModel<Editeur>();
                    modelEditeur.BDD_Delete(int.Parse(currentIdEditeur));
                    currentIdEditeur = null;
                    RefreshTabLivre();
                }
                else
                {
                    MessageBox.Show("Vous devez d'abord cliquer sur un editeur", "Information ");
                }
                RefreshTabEditeur();
                RefreshTabExemplaire();
                RefreshTabLivre();
            }
        }

        /// <summary>                                                                           <summary>
        /// --------------------------- FIN PARTIE TAB AUTEUR/EDITEUR --------------------------------///
        /// --------------------------- DEBUT PARTIE TAB LIVRE ---------------------------------------///
        /// </summary>                                                                          <summary>

        private void RefreshTabLivre()
        {
            grdLivre.Items.Clear();
            ListViewItem item;
            Livre modelLivre = ModelReq.LoadModel<Livre>();
            foreach (Livre unLivre in modelLivre.BDD_Read<Livre>())
            {
                item = new ListViewItem(unLivre.id);
                item.SubItems.Add(unLivre.titre);
                item.SubItems.Add(unLivre.ISBN);
                item.SubItems.Add(unLivre.id_Auteur.nom + " " + unLivre.id_Auteur.prenom);
                item.SubItems.Add(unLivre.id_Editeur.nom + " " + unLivre.id_Editeur.prenom);
                string date = String.Format("{0:dd/MM/yyyy}", unLivre.dateEdition);
                item.SubItems.Add(date);
                item.SubItems.Add(unLivre.nbPages.ToString());
                ExemplaireControlleur Econtroll = new ExemplaireControlleur();
                string quantite = Econtroll.CountExemplaire(unLivre.id);
                item.SubItems.Add(quantite);
                grdLivre.Items.Add(item);
                //Permet d'alterner la couleur de la ligne
                item.BackColor = item.Index % 2 == 0 ? Color.Chartreuse : Color.LightSkyBlue;
            }

            List<Editeur> lesEditeurs = new List<Editeur>();
            Editeur modelEditeur = ModelReq.LoadModel<Editeur>();
            lesEditeurs = modelEditeur.BDD_Read<Editeur>();
            dropDownEditeur.Items.Clear();
            ComboBoxItem itemEditeur;
            foreach (Editeur unEditeur in lesEditeurs)
            {
                itemEditeur = new ComboBoxItem();
                itemEditeur.Text = unEditeur.nom + " " + unEditeur.prenom;
                itemEditeur.Value = unEditeur.id;
                dropDownEditeur.Items.Add(itemEditeur);
            }

            List<Auteur> lesAuteurs = new List<Auteur>();
            Auteur modelAuteur = ModelReq.LoadModel<Auteur>();
            lesAuteurs = modelAuteur.BDD_Read<Auteur>();
            dropDownAuteur.Items.Clear();
            ComboBoxItem itemAuteur;
            foreach (Auteur unAuteur in lesAuteurs)
            {
                itemAuteur = new ComboBoxItem();
                itemAuteur.Text = unAuteur.nom + " " + unAuteur.prenom;
                itemAuteur.Value = unAuteur.id;
                dropDownAuteur.Items.Add(itemAuteur);
            }
        }

        private void dropDownAuteur_MouseClick(object sender, MouseEventArgs e)
        {
            List<Auteur> lesAuteurs = new List<Auteur>();
            Auteur modelAuteur = ModelReq.LoadModel<Auteur>();
            lesAuteurs = modelAuteur.BDD_Read<Auteur>();
            dropDownAuteur.Items.Clear();
            ComboBoxItem item;
            foreach (Auteur unAuteur in lesAuteurs)
            {
                item = new ComboBoxItem();
                item.Text = unAuteur.nom + " " + unAuteur.prenom;
                item.Value = unAuteur.id;
                dropDownAuteur.Items.Add(item);
            }
        }

        private void dropDownEditeur_MouseClick(object sender, MouseEventArgs e)
        {
            List<Editeur> lesEditeurs = new List<Editeur>();
            Editeur modelEditeur = ModelReq.LoadModel<Editeur>();
            lesEditeurs = modelEditeur.BDD_Read<Editeur>();
            dropDownEditeur.Items.Clear();
            ComboBoxItem item;
            foreach (Editeur unEditeur in lesEditeurs)
            {
                item = new ComboBoxItem();
                item.Text = unEditeur.nom + " " + unEditeur.prenom;
                item.Value = unEditeur.id;
                dropDownEditeur.Items.Add(item);
            }
        }

        private void grdLivre_SelectedIndexChanged(object sender, EventArgs e)
        {

            for (int i = 0; i < grdLivre.Items.Count; i++)
                if (grdLivre.Items[i].Selected == true)
                {
                    currentIdLivre = grdLivre.Items[i].SubItems[0].Text;
                    txtTitre.Text = grdLivre.Items[i].SubItems[1].Text;
                    txtISBN.Text = grdLivre.Items[i].SubItems[2].Text;
                    comboBoxIdAuteur = grdLivre.Items[i].SubItems[3].Text;
                    for (int j = 0; j < dropDownAuteur.Items.Count; j++)
                    {
                        if ((dropDownAuteur.Items[j] as ComboBoxItem).Text.ToString() == comboBoxIdAuteur)
                        {
                            dropDownAuteur.SelectedIndex = j;
                        }
                    }
                    comboBoxIdEditeur = grdLivre.Items[i].SubItems[4].Text;
                    for (int j = 0; j < dropDownEditeur.Items.Count; j++)
                    {
                        if ((dropDownEditeur.Items[j] as ComboBoxItem).Text.ToString() == comboBoxIdEditeur)
                        {
                            dropDownEditeur.SelectedIndex = j;
                        }
                    }
                    txtDateEdition.Text = grdLivre.Items[i].SubItems[5].Text;
                    dateTPickerLivre.Value = Convert.ToDateTime(txtDateEdition.Text);
                    txtNbPage.Text = grdLivre.Items[i].SubItems[6].Text;
                }
        }

        private void btnAddLivre_Click(object sender, EventArgs e)
        {
            if ((txtISBN.Text != string.Empty) && (txtNbPage.Text != string.Empty) && (txtDateEdition.Text != string.Empty) && (txtTitre.Text != string.Empty) && dropDownAuteur.SelectedItem != null && dropDownEditeur.SelectedItem != null)
            {
                try
                {
                    int idAuteur = int.Parse((dropDownAuteur.SelectedItem as ComboBoxItem).Value.ToString());
                    int idEditeur = int.Parse((dropDownEditeur.SelectedItem as ComboBoxItem).Value.ToString());
                    Livre unLivre = new Livre(txtTitre.Text, txtISBN.Text.ToUpper(), idEditeur, idAuteur, Convert.ToDateTime(txtDateEdition.Text), int.Parse(txtNbPage.Text));
                    LivreControlleur Lcontroll = new LivreControlleur();
                    Lcontroll.AjouterLivre(unLivre);
                    RefreshTabLivre();
                }
                catch
                {
                    MessageBox.Show("Erreur champs mal renseignés");
                }
            }
            else
            {
                MessageBox.Show("Les champs ne sont pas tous complétés");
            }
        }

        private void btnEditLivre_Click(object sender, EventArgs e)
        {
            if ((txtISBN.Text != string.Empty) && (txtNbPage.Text != string.Empty) && (txtDateEdition.Text != string.Empty) && (txtTitre.Text != string.Empty) && dropDownAuteur.SelectedItem != null && dropDownEditeur.SelectedItem != null)
            {
                int idAuteur = int.Parse((dropDownAuteur.SelectedItem as ComboBoxItem).Value.ToString());
                int idEditeur = int.Parse((dropDownEditeur.SelectedItem as ComboBoxItem).Value.ToString());
                int nbPages;
                int.TryParse(txtNbPage.Text, out nbPages);
                Livre unLivre = new Livre(txtTitre.Text, txtISBN.Text.ToUpper(), idEditeur, idAuteur, Convert.ToDateTime(txtDateEdition.Text), nbPages, currentIdLivre);
                try
                {
                    LivreControlleur Lcontroll = new LivreControlleur();
                    Lcontroll.AjouterLivre(unLivre);
                    RefreshTabLivre();
                    RefreshTabExemplaire();
                }
                catch
                {
                    MessageBox.Show("Erreur champs mal renseignés");
                }
            }
            else
            {
                MessageBox.Show("Les champs ne sont pas tous complétés");
            }
        }

        private void btnDelLivre_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Voulez vous vraiment suprimer ce livre ?\n Cela suprimera aussi les exemplaires associés!", "Confirmation de supression", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (currentIdLivre != null)
                {
                    Livre modelLivre = ModelReq.LoadModel<Livre>();
                    modelLivre.BDD_Delete(int.Parse(currentIdLivre));
                    currentIdLivre = null;
                }
                else
                {
                    MessageBox.Show("Vous devez d'abord cliquer sur un livre", "Information ");
                }
                RefreshTabLivre();
                RefreshTabExemplaire();
            }
        }

        /// <summary>                                                                           <summary>
        /// --------------------------- FIN PARTIE TAB LIVRE -----------------------------------------///
        /// --------------------------- DEBUT PARTIE TAB EXEMPLAIRE ----------------------------------///
        /// </summary>                                                                          <summary>

        private void RefreshTabExemplaire()
        {
            grdExemplaire.Items.Clear();
            ListViewItem item;
            Exemplaire modelExemplaire = ModelReq.LoadModel<Exemplaire>();
            foreach (Exemplaire unExemplaire in modelExemplaire.BDD_Read<Exemplaire>())
            {
                item = new ListViewItem(unExemplaire.id);
                item.SubItems.Add(unExemplaire.id_Livre.id);
                item.SubItems.Add(unExemplaire.id_Livre.titre);
                item.SubItems.Add(unExemplaire.etat);
                string date = String.Format("{0:dd/MM/yyyy}", unExemplaire.dateAcquisition);
                item.SubItems.Add(date);
                grdExemplaire.Items.Add(item);
                //Permet d'alterner la couleur de la ligne
                item.BackColor = item.Index % 2 == 0 ? Color.Chartreuse : Color.LightSkyBlue;
            }
            List<Livre> lesLivres = new List<Livre>();
            Livre modelLivre = ModelReq.LoadModel<Livre>();
            lesLivres = modelLivre.BDD_Read<Livre>();
            dropDownLivre.Items.Clear();
            ComboBoxItem itemLivre;
            foreach (Livre unLivre in lesLivres)
            {
                itemLivre = new ComboBoxItem();
                itemLivre.Text = unLivre.id + " : " + unLivre.titre;
                itemLivre.Value = unLivre.id;
                dropDownLivre.Items.Add(itemLivre);
            }
            currentIdExemplaire = null;
        }

        private void grdExemplaire_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < grdExemplaire.Items.Count; i++)
                if (grdExemplaire.Items[i].Selected == true)
                {
                    currentIdExemplaire = grdExemplaire.Items[i].SubItems[0].Text;
                    txtDateAcquis.Text = grdExemplaire.Items[i].SubItems[4].Text;
                    dateTPickerExemplaire.Value = Convert.ToDateTime(txtDateAcquis.Text);
                    string comboBoxEtat = grdExemplaire.Items[i].SubItems[3].Text;
                    for (int j = 0; j < dropDownEtat.Items.Count; j++)
                    {
                        if (dropDownEtat.Items[j].ToString() == comboBoxEtat)
                        {
                            dropDownEtat.SelectedIndex = j;
                        }
                    }

                    string comboBoxIdLivre = grdExemplaire.Items[i].SubItems[1].Text;
                    for (int j = 0; j < dropDownLivre.Items.Count; j++)
                    {
                        if ((dropDownLivre.Items[j] as ComboBoxItem).Value.ToString() == comboBoxIdLivre)
                        {
                            dropDownLivre.SelectedIndex = j;
                        }
                    }
                }
        }

        private void dropDownLivre_MouseClick(object sender, MouseEventArgs e)
        {
            List<Livre> lesLivres = new List<Livre>();
            Livre modelLivre = ModelReq.LoadModel<Livre>();
            lesLivres = modelLivre.BDD_Read<Livre>();
            dropDownLivre.Items.Clear();
            ComboBoxItem item;
            foreach (Livre unLivre in lesLivres)
            {
                item = new ComboBoxItem();
                item.Text = unLivre.id + " : " + unLivre.titre;
                item.Value = unLivre.id;
                dropDownLivre.Items.Add(item);
            }
        }

        private void btnAddExemplaire_Click(object sender, EventArgs e)
        {
            if ((txtDateAcquis.Text != string.Empty) && (dropDownLivre.SelectedItem != null) && (dropDownEtat.SelectedItem != null))
            {
                try
                {
                    int idLivre = int.Parse((dropDownLivre.SelectedItem as ComboBoxItem).Value.ToString());
                    string Etat = dropDownEtat.SelectedItem.ToString();
                    Exemplaire unExemplaire = new Exemplaire(idLivre, Etat, Convert.ToDateTime(txtDateAcquis.Text));
                    ExemplaireControlleur Excontroll = new ExemplaireControlleur();
                    Excontroll.AjouterExemplaire(unExemplaire);
                    RefreshTabExemplaire();
                    RefreshTabLivre();
                }
                catch
                {
                    MessageBox.Show("Erreur champs mal renseignés");
                }
            }
            else
            {
                MessageBox.Show("Les champs ne sont pas tous complétés");
            }
        }

        private void btnEditExemplaire_Click(object sender, EventArgs e)
        {
            if ((txtDateAcquis.Text != string.Empty) && (dropDownLivre.SelectedItem != null) && (dropDownEtat.SelectedItem != null))
            {
                try
                {
                    int idLivre = int.Parse((dropDownLivre.SelectedItem as ComboBoxItem).Value.ToString());
                    string Etat = dropDownEtat.SelectedItem.ToString();
                    Exemplaire unExemplaire = new Exemplaire(idLivre, Etat, Convert.ToDateTime(txtDateAcquis.Text), currentIdExemplaire);
                    ExemplaireControlleur Excontroll = new ExemplaireControlleur();
                    Excontroll.AjouterExemplaire(unExemplaire);
                    RefreshTabExemplaire();
                    RefreshTabLivre();
                }
                catch
                {
                    MessageBox.Show("Erreur champs mal renseignés");
                }
            }
            else
            {
                MessageBox.Show("Les champs ne sont pas tous complétés");
            }
        }

        private void btnDelExemplaire_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Voulez vous vraiment suprimer cet exemplaire ?", "Confirmation de supression", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (currentIdExemplaire != null)
                {
                    Exemplaire modelExemplaire = ModelReq.LoadModel<Exemplaire>();
                    modelExemplaire.BDD_Delete(int.Parse(currentIdExemplaire));
                    currentIdExemplaire = null;
                }
                else
                {
                    MessageBox.Show("Vous devez d'abord cliquer sur un Exemplaire", "Information ");
                }
                RefreshTabExemplaire();
                RefreshTabLivre();
            }
        }
        /// <summary>                                                                           <summary>
        /// --------------------------- FIN PARTIE TAB EXEMPLAIRE ------------------------------------///
        /// --------------------------- DEBUT PARTIE RETOUR -----------------------------------------///
        /// </summary>                                                                          <summary>

        private void RefreshTabEmprunt()
        {
            grdEmprunt.Items.Clear();
            ListViewItem item;
            Emprunt modelEmprunt = ModelReq.LoadModel<Emprunt>();
            EmpruntControlleur EmControll = new EmpruntControlleur();
            foreach (Emprunt unEmprunt in modelEmprunt.BDD_Read<Emprunt>(default(int), null, null, " GROUP BY id_Adherent "))
            {
                if(unEmprunt != null)
                {
                string idAdherent = unEmprunt.id_Adherent.id;
                item = new ListViewItem(idAdherent);
                item.SubItems.Add(unEmprunt.id_Adherent.nom);
                item.SubItems.Add(unEmprunt.id_Adherent.prenom);
                item.SubItems.Add(EmControll.CountEmprunt(idAdherent));
                grdEmprunt.Items.Add(item);
                //Permet d'alterner la couleur de la ligne
                item.BackColor = item.Index % 2 == 0 ? Color.Chartreuse : Color.LightSkyBlue;
                }
            }
        }
        private void RefreshListeEmprunt()
        {
            grdListEmprunt.Items.Clear();
            currentIdLivre = null;
            currentIdExemplaire = null;
            if (currentIdEmprunteur != string.Empty)
            {
                ListViewItem item;
                //Emprunt modelEmprunt = ModelReq.LoadModel<Emprunt>();
                EmpruntControlleur EmControll = new EmpruntControlleur();
                foreach (Livre unLivre in EmControll.ListeEmpruntAdherent(currentIdEmprunteur))
                {
                    if (unLivre != null)
                    {
                        item = new ListViewItem();
                        item.SubItems.Add(unLivre.ISBN);
                        grdListEmprunt.Items.Add(item);
                        //Permet d'alterner la couleur de la ligne
                        item.BackColor = item.Index % 2 == 0 ? Color.Chartreuse : Color.LightSkyBlue;
                    }
                }
                Emprunt modelEmprunt = ModelReq.LoadModel<Emprunt>();
                int i = 0;
                foreach (Emprunt unEmprunt in modelEmprunt.BDD_Read<Emprunt>(default(int), "id_Adherent = '" + currentIdEmprunteur + "'", null))
                {
                    if (unEmprunt != null)
                    {
                        string dateEmprunt = String.Format("{0:dd/MM/yyyy}", unEmprunt.dateEmprunt);
                        grdListEmprunt.Items[i].SubItems.Add(dateEmprunt);
                        string dateRetour = String.Format("{0:dd/MM/yyyy}", unEmprunt.dateRetour);
                        grdListEmprunt.Items[i].SubItems.Add(dateRetour);
                        grdListEmprunt.Items[i].SubItems.Add(unEmprunt.id_Exemplaire.id);
                        grdListEmprunt.Items[i].SubItems[0].Text = unEmprunt.id_Livre.id;
                        i++;
                    }
                }
            }
        }

        private void grdEmprunt_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < grdEmprunt.Items.Count; i++)
            {
                if (grdEmprunt.Items[i].Selected == true)
                {
                    currentIdEmprunteur = grdEmprunt.Items[i].SubItems[0].Text;
                    currentIdLivre = null;

                }
            }
            RefreshListeEmprunt();
        }

        private void grdListEmprunt_SelectedIndexChanged(object sender, EventArgs e)
        {

            for (int i = 0; i < grdListEmprunt.Items.Count; i++)
            {
                if (grdListEmprunt.Items[i].Selected == true)
                {
                    currentIdLivre = grdListEmprunt.Items[i].SubItems[0].Text;
                    currentIdExemplaire = grdListEmprunt.Items[i].SubItems[4].Text;
                }
            }
        }

        private void btnRestituer_Click(object sender, EventArgs e)
        {
            if (currentIdLivre != null && currentIdExemplaire != null && currentIdEmprunteur != null)
            {
                string condition = " id_Livre =" + currentIdLivre + " AND id_Adherent = " + currentIdEmprunteur + " AND id_Exemplaire = " + currentIdExemplaire;
                Emprunt modelEprunt = ModelReq.LoadModel<Emprunt>();
                modelEmprunt.BDD_Delete(default(int), condition);
            }
            else
            {
                MessageBox.Show("Vous devez d'abord cliquer sur un livre dans la liste", "Information ");
            }
            RefreshListeEmprunt();
            RefreshTabEmprunt();
        }

        /// <summary>                                                                           <summary>
        /// --------------------------- FIN PARTIE TAB RETOUR ------------------------------------///
        /// --------------------------- DEBUT PARTIE EMPRUNT -----------------------------------------///
        /// </summary>                                                                          <summary> 


        private void RefreshTabExemplairePret()
        {
            grdListExemplaires.Items.Clear();
            if (txtLivre.Text != string.Empty)
            {

                ListViewItem item;
                Exemplaire modelExemplaire = ModelReq.LoadModel<Exemplaire>();
                foreach (Exemplaire unExemplaire in modelExemplaire.BDD_Read<Exemplaire>(default(int), "id_Livre = " + txtLivre.Text))
                {
                    //currentIdExemplaire = unExemplaire.id;
                    item = new ListViewItem(unExemplaire.id);
                    item.SubItems.Add(unExemplaire.etat);
                    grdListExemplaires.Items.Add(item);
                    //Permet d'alterner la couleur de la ligne
                    item.BackColor = item.Index % 2 == 0 ? Color.Chartreuse : Color.LightSkyBlue;
                }
                Emprunt modelEmprunt = ModelReq.LoadModel<Emprunt>();
                int i = 0;
                foreach (Emprunt unEmprunt in modelEmprunt.BDD_Read<Emprunt>(default(int), "id_Livre = '" + txtLivre.Text + "'", null))
                {
                    if (unEmprunt != null)
                    {
                        string dateRetour = String.Format("{0:dd/MM/yyyy}", unEmprunt.dateRetour);
                        if (dateRetour != string.Empty)
                        {
                            grdListExemplaires.Items[i].SubItems.Add(dateRetour);
                        }
                        i++;
                    }

                }
            }
        }

        private void dropDownLivrePret_MouseClick(object sender, MouseEventArgs e)
        {
            {
                // empeche l'utilisateur de choisir le mauvais exemplaire du livre
                dropDownExemplaire.Items.Clear();
                txtExemplaire.Text = string.Empty;

                List<Livre> lesLivres = new List<Livre>();
                Livre modelLivre = ModelReq.LoadModel<Livre>();
                lesLivres = modelLivre.BDD_Read<Livre>();
                dropDownLivrePret.Items.Clear();
                ComboBoxItem item;
                foreach (Livre unLivre in lesLivres)
                {
                    item = new ComboBoxItem();
                    item.Text = unLivre.id + " : " + unLivre.titre;
                    item.Value = unLivre.id;
                    dropDownLivrePret.Items.Add(item);
                }
            }
        }

        private void dropDownExemplaire_MouseClick(object sender, MouseEventArgs e)
        {
            if (txtLivre.Text != string.Empty)
            {
                List<Exemplaire> lesExemplaires = new List<Exemplaire>();
                Exemplaire modelExemplaire = ModelReq.LoadModel<Exemplaire>();
                // Condition currentIdLivre dependance précédent dropdown
                lesExemplaires = modelExemplaire.BDD_Read<Exemplaire>(default(int), " id_Livre = " + txtLivre.Text);
                dropDownExemplaire.Items.Clear();
                ComboBoxItem item;
                foreach (Exemplaire unExemplaire in lesExemplaires)
                {
                    item = new ComboBoxItem();
                    item.Text = unExemplaire.id + " : " + unExemplaire.etat;
                    item.Value = unExemplaire.id;
                    dropDownExemplaire.Items.Add(item);
                }
            }
            else
            {
                MessageBox.Show("Vous devez choisir le livre avant.", "Information");
            }
        }

        private void dropDownAdherent_MouseClick(object sender, MouseEventArgs e)
        {
            {
                List<Adherent> lesAdherents = new List<Adherent>();
                Adherent modelAdherent = ModelReq.LoadModel<Adherent>();
                lesAdherents = modelAdherent.BDD_Read<Adherent>();
                dropDownAdherent.Items.Clear();
                ComboBoxItem item;
                foreach (Adherent unAdherent in lesAdherents)
                {
                    item = new ComboBoxItem();
                    item.Text = unAdherent.nom + " : " + unAdherent.prenom;
                    item.Value = unAdherent.id;
                    dropDownAdherent.Items.Add(item);
                }
            }
        }

        private void dateTPickerPret_ValueChanged(object sender, EventArgs e)
        {
            txtDatePret.Text = dateTPickerPret.Value.ToShortDateString();
        }

        private void dropDownLivrePret_TextChanged(object sender, EventArgs e)
        {
            if (dropDownLivrePret.Text != string.Empty)
            {
                txtLivre.Text = ((dropDownLivrePret.SelectedItem as ComboBoxItem).Value.ToString());
                RefreshTabExemplairePret();
            }
            else
            {
                grdListExemplaires.Items.Clear();
                dropDownExemplaire.Items.Clear();
            }
        }

        private void dropDownExemplaire_TextChanged(object sender, EventArgs e)
        {
            if (dropDownExemplaire.Text != string.Empty)
            {
                txtExemplaire.Text = ((dropDownExemplaire.SelectedItem as ComboBoxItem).Value.ToString());
            }
        }

        private void dropDownAdherent_TextChanged(object sender, EventArgs e)
        {
            if (dropDownAdherent.Text != string.Empty)
            {
                txtAdherent.Text = ((dropDownAdherent.SelectedItem as ComboBoxItem).Value.ToString());
            }
        }

        private void btnPret_Click(object sender, EventArgs e)
        {
            if (txtAdherent.Text != string.Empty && txtLivre.Text != string.Empty && txtExemplaire.Text != string.Empty)
            {
                try
                {
                    string txtDateNow = DateTime.Now.ToShortDateString();
                    Emprunt unEmprunt = new Emprunt(int.Parse(txtLivre.Text), int.Parse(txtExemplaire.Text), int.Parse(txtAdherent.Text), Convert.ToDateTime(txtDateNow), Convert.ToDateTime(txtDatePret.Text));
                    EmpruntControlleur Acontroll = new EmpruntControlleur();
                    if (Acontroll.AjouterEmprunt(unEmprunt))
                    {
                        MessageBox.Show("Emprunt enregistré", "Information");
                        RefreshTabEmprunt();
                        RefreshTabExemplairePret();
                    }
                    else
                    {
                        MessageBox.Show("Cet adhérent a déjà un exemplaire du même livre", "Information");
                    }

                }
                catch
                {
                    MessageBox.Show("Erreur verrifier les champs, l'exemplaire est peut être déjà emprunté");
                }
            }
            else
            {
                MessageBox.Show("Vous devez selectioner tous les champs", "Information");
            }
        }

        private void grdListExemplaires_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < grdListExemplaires.Items.Count; i++)
            {
                if (grdListExemplaires.Items[i].Selected == true)
                {
                    txtExemplaire.Text = grdListExemplaires.Items[i].SubItems[0].Text;
                }
            }
        }
    }
}
