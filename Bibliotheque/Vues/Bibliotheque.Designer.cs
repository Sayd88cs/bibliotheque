﻿namespace Bibliotheque
{
    partial class Bibliotheque
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Bibliotheque));
            this.tabControlAdmin = new System.Windows.Forms.TabControl();
            this.tabPageAdherent = new System.Windows.Forms.TabPage();
            this.grpDetailsAdherent = new System.Windows.Forms.GroupBox();
            this.dateTPickerAdherent = new System.Windows.Forms.DateTimePicker();
            this.txtCP = new System.Windows.Forms.TextBox();
            this.lbCP = new System.Windows.Forms.Label();
            this.txtVille = new System.Windows.Forms.TextBox();
            this.lbVille = new System.Windows.Forms.Label();
            this.grbSex = new System.Windows.Forms.GroupBox();
            this.rdFemme = new System.Windows.Forms.RadioButton();
            this.rdHomme = new System.Windows.Forms.RadioButton();
            this.txtAdresse = new System.Windows.Forms.TextBox();
            this.lbAdresse = new System.Windows.Forms.Label();
            this.txtDateNaiss = new System.Windows.Forms.TextBox();
            this.lbDateNaiss = new System.Windows.Forms.Label();
            this.txtPrenom = new System.Windows.Forms.TextBox();
            this.lbPrenom = new System.Windows.Forms.Label();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.lbNom = new System.Windows.Forms.Label();
            this.lbListeAdherent = new System.Windows.Forms.Label();
            this.grdAdherent = new System.Windows.Forms.ListView();
            this.colHeaderID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderNom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderPrenom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderSexe = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderDateNaiss = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderAdresse = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderVille = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderCP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.tabPageAuteurEditeur = new System.Windows.Forms.TabPage();
            this.lbListeEditeurs = new System.Windows.Forms.Label();
            this.lbListeAuteurs = new System.Windows.Forms.Label();
            this.btnEditEditeur = new System.Windows.Forms.Button();
            this.btnAddEditeur = new System.Windows.Forms.Button();
            this.btnDelEditeur = new System.Windows.Forms.Button();
            this.grdEditeur = new System.Windows.Forms.ListView();
            this.colHeaderEditeursID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderEditeursNom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderEditeursPrenom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.grpDetailsEditeur = new System.Windows.Forms.GroupBox();
            this.txtPrenomEditeur = new System.Windows.Forms.TextBox();
            this.lbPrenomEditeur = new System.Windows.Forms.Label();
            this.txtNomEditeur = new System.Windows.Forms.TextBox();
            this.lbNomEditeur = new System.Windows.Forms.Label();
            this.btnEditAuteur = new System.Windows.Forms.Button();
            this.btnAddAuteur = new System.Windows.Forms.Button();
            this.btnDelAuteur = new System.Windows.Forms.Button();
            this.grdAuteur = new System.Windows.Forms.ListView();
            this.colHeaderAuteursID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderAuteursNom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderAuteursPrenom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.grpDetailsAuteur = new System.Windows.Forms.GroupBox();
            this.txtPrenomAuteur = new System.Windows.Forms.TextBox();
            this.lbPrenomAuteur = new System.Windows.Forms.Label();
            this.txtNomAuteur = new System.Windows.Forms.TextBox();
            this.lbNomAuteur = new System.Windows.Forms.Label();
            this.tabPageLivre = new System.Windows.Forms.TabPage();
            this.lbListeLivres = new System.Windows.Forms.Label();
            this.grdLivre = new System.Windows.Forms.ListView();
            this.colHeaderLivresID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderLivresTitre = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderLivresISBN = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderLivresAuteur = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderLivresEditeur = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderLivresDateE = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderLivreNbP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderLivreQte = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnEditLivre = new System.Windows.Forms.Button();
            this.grpDetailsLivre = new System.Windows.Forms.GroupBox();
            this.dateTPickerLivre = new System.Windows.Forms.DateTimePicker();
            this.dropDownEditeur = new System.Windows.Forms.ComboBox();
            this.lbEditeur = new System.Windows.Forms.Label();
            this.txtNbPage = new System.Windows.Forms.TextBox();
            this.lbNbPages = new System.Windows.Forms.Label();
            this.dropDownAuteur = new System.Windows.Forms.ComboBox();
            this.txtDateEdition = new System.Windows.Forms.TextBox();
            this.lbDateEdition = new System.Windows.Forms.Label();
            this.txtISBN = new System.Windows.Forms.TextBox();
            this.lbISBN = new System.Windows.Forms.Label();
            this.lbAuteur = new System.Windows.Forms.Label();
            this.txtTitre = new System.Windows.Forms.TextBox();
            this.lbTitre = new System.Windows.Forms.Label();
            this.btnAddLivre = new System.Windows.Forms.Button();
            this.btnDelLivre = new System.Windows.Forms.Button();
            this.tabPageExemplaire = new System.Windows.Forms.TabPage();
            this.lbListeExemplaires = new System.Windows.Forms.Label();
            this.grdExemplaire = new System.Windows.Forms.ListView();
            this.colHeaderExemplaireID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderExemplaireLivreID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderExemplaireTitre = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderExemplaireEtat = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderExemplaireDateAcquis = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnEditExemplaire = new System.Windows.Forms.Button();
            this.grpDetailsExemplaire = new System.Windows.Forms.GroupBox();
            this.dateTPickerExemplaire = new System.Windows.Forms.DateTimePicker();
            this.dropDownEtat = new System.Windows.Forms.ComboBox();
            this.lbEtat = new System.Windows.Forms.Label();
            this.dropDownLivre = new System.Windows.Forms.ComboBox();
            this.lbEtat2 = new System.Windows.Forms.Label();
            this.txtDateAcquis = new System.Windows.Forms.TextBox();
            this.lbDateAcquis = new System.Windows.Forms.Label();
            this.btnAddExemplaire = new System.Windows.Forms.Button();
            this.btnDelExemplaire = new System.Windows.Forms.Button();
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.toolStripGestionEmprunts = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripAdministration = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControlGestion = new System.Windows.Forms.TabControl();
            this.tabPageRetour = new System.Windows.Forms.TabPage();
            this.btnRestituer = new System.Windows.Forms.Button();
            this.grpDetailsEmprunt = new System.Windows.Forms.GroupBox();
            this.grdListEmprunt = new System.Windows.Forms.ListView();
            this.colHeaderEmprId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderEmprISBN = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderEmprDEmpr = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderEmprDRetour = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lbEmprunt = new System.Windows.Forms.Label();
            this.grdEmprunt = new System.Windows.Forms.ListView();
            this.ccolHeaderEmprID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderEmprNom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderEmprPrenom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderEmprQte = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPageEmprunt = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.lbidEPret = new System.Windows.Forms.Label();
            this.lbidLPret = new System.Windows.Forms.Label();
            this.lbidAPret = new System.Windows.Forms.Label();
            this.txtExemplaire = new System.Windows.Forms.TextBox();
            this.dropDownExemplaire = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnPret = new System.Windows.Forms.Button();
            this.dateTPickerPret = new System.Windows.Forms.DateTimePicker();
            this.txtDatePret = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLivre = new System.Windows.Forms.TextBox();
            this.txtAdherent = new System.Windows.Forms.TextBox();
            this.dropDownLivrePret = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dropDownAdherent = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.grpDetailsPretExemplaire = new System.Windows.Forms.GroupBox();
            this.grdListExemplaires = new System.Windows.Forms.ListView();
            this.colHeaderPretID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderPretEtat = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderPretDRetour = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabControlAdmin.SuspendLayout();
            this.tabPageAdherent.SuspendLayout();
            this.grpDetailsAdherent.SuspendLayout();
            this.grbSex.SuspendLayout();
            this.tabPageAuteurEditeur.SuspendLayout();
            this.grpDetailsEditeur.SuspendLayout();
            this.grpDetailsAuteur.SuspendLayout();
            this.tabPageLivre.SuspendLayout();
            this.grpDetailsLivre.SuspendLayout();
            this.tabPageExemplaire.SuspendLayout();
            this.grpDetailsExemplaire.SuspendLayout();
            this.menuStripMain.SuspendLayout();
            this.tabControlGestion.SuspendLayout();
            this.tabPageRetour.SuspendLayout();
            this.grpDetailsEmprunt.SuspendLayout();
            this.tabPageEmprunt.SuspendLayout();
            this.grpDetailsPretExemplaire.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlAdmin
            // 
            this.tabControlAdmin.Controls.Add(this.tabPageAdherent);
            this.tabControlAdmin.Controls.Add(this.tabPageAuteurEditeur);
            this.tabControlAdmin.Controls.Add(this.tabPageLivre);
            this.tabControlAdmin.Controls.Add(this.tabPageExemplaire);
            resources.ApplyResources(this.tabControlAdmin, "tabControlAdmin");
            this.tabControlAdmin.Name = "tabControlAdmin";
            this.tabControlAdmin.SelectedIndex = 0;
            // 
            // tabPageAdherent
            // 
            this.tabPageAdherent.Controls.Add(this.grpDetailsAdherent);
            this.tabPageAdherent.Controls.Add(this.lbListeAdherent);
            this.tabPageAdherent.Controls.Add(this.grdAdherent);
            this.tabPageAdherent.Controls.Add(this.btnEdit);
            this.tabPageAdherent.Controls.Add(this.btnAdd);
            this.tabPageAdherent.Controls.Add(this.btnDel);
            resources.ApplyResources(this.tabPageAdherent, "tabPageAdherent");
            this.tabPageAdherent.Name = "tabPageAdherent";
            this.tabPageAdherent.UseVisualStyleBackColor = true;
            // 
            // grpDetailsAdherent
            // 
            this.grpDetailsAdherent.Controls.Add(this.dateTPickerAdherent);
            this.grpDetailsAdherent.Controls.Add(this.txtCP);
            this.grpDetailsAdherent.Controls.Add(this.lbCP);
            this.grpDetailsAdherent.Controls.Add(this.txtVille);
            this.grpDetailsAdherent.Controls.Add(this.lbVille);
            this.grpDetailsAdherent.Controls.Add(this.grbSex);
            this.grpDetailsAdherent.Controls.Add(this.txtAdresse);
            this.grpDetailsAdherent.Controls.Add(this.lbAdresse);
            this.grpDetailsAdherent.Controls.Add(this.txtDateNaiss);
            this.grpDetailsAdherent.Controls.Add(this.lbDateNaiss);
            this.grpDetailsAdherent.Controls.Add(this.txtPrenom);
            this.grpDetailsAdherent.Controls.Add(this.lbPrenom);
            this.grpDetailsAdherent.Controls.Add(this.txtNom);
            this.grpDetailsAdherent.Controls.Add(this.lbNom);
            this.grpDetailsAdherent.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            resources.ApplyResources(this.grpDetailsAdherent, "grpDetailsAdherent");
            this.grpDetailsAdherent.Name = "grpDetailsAdherent";
            this.grpDetailsAdherent.TabStop = false;
            // 
            // dateTPickerAdherent
            // 
            this.dateTPickerAdherent.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.dateTPickerAdherent, "dateTPickerAdherent");
            this.dateTPickerAdherent.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTPickerAdherent.MaxDate = new System.DateTime(2300, 12, 31, 0, 0, 0, 0);
            this.dateTPickerAdherent.MinDate = new System.DateTime(1850, 1, 1, 0, 0, 0, 0);
            this.dateTPickerAdherent.Name = "dateTPickerAdherent";
            this.dateTPickerAdherent.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateTPickerAdherent.ValueChanged += new System.EventHandler(this.dateTPickerAdherent_ValueChanged);
            // 
            // txtCP
            // 
            resources.ApplyResources(this.txtCP, "txtCP");
            this.txtCP.Name = "txtCP";
            // 
            // lbCP
            // 
            resources.ApplyResources(this.lbCP, "lbCP");
            this.lbCP.Name = "lbCP";
            // 
            // txtVille
            // 
            resources.ApplyResources(this.txtVille, "txtVille");
            this.txtVille.Name = "txtVille";
            // 
            // lbVille
            // 
            resources.ApplyResources(this.lbVille, "lbVille");
            this.lbVille.Name = "lbVille";
            // 
            // grbSex
            // 
            this.grbSex.Controls.Add(this.rdFemme);
            this.grbSex.Controls.Add(this.rdHomme);
            resources.ApplyResources(this.grbSex, "grbSex");
            this.grbSex.ForeColor = System.Drawing.SystemColors.MenuText;
            this.grbSex.Name = "grbSex";
            this.grbSex.TabStop = false;
            // 
            // rdFemme
            // 
            resources.ApplyResources(this.rdFemme, "rdFemme");
            this.rdFemme.Name = "rdFemme";
            // 
            // rdHomme
            // 
            resources.ApplyResources(this.rdHomme, "rdHomme");
            this.rdHomme.Name = "rdHomme";
            // 
            // txtAdresse
            // 
            resources.ApplyResources(this.txtAdresse, "txtAdresse");
            this.txtAdresse.Name = "txtAdresse";
            // 
            // lbAdresse
            // 
            resources.ApplyResources(this.lbAdresse, "lbAdresse");
            this.lbAdresse.Name = "lbAdresse";
            // 
            // txtDateNaiss
            // 
            resources.ApplyResources(this.txtDateNaiss, "txtDateNaiss");
            this.txtDateNaiss.Name = "txtDateNaiss";
            // 
            // lbDateNaiss
            // 
            resources.ApplyResources(this.lbDateNaiss, "lbDateNaiss");
            this.lbDateNaiss.Name = "lbDateNaiss";
            // 
            // txtPrenom
            // 
            resources.ApplyResources(this.txtPrenom, "txtPrenom");
            this.txtPrenom.Name = "txtPrenom";
            // 
            // lbPrenom
            // 
            resources.ApplyResources(this.lbPrenom, "lbPrenom");
            this.lbPrenom.Name = "lbPrenom";
            // 
            // txtNom
            // 
            resources.ApplyResources(this.txtNom, "txtNom");
            this.txtNom.Name = "txtNom";
            // 
            // lbNom
            // 
            resources.ApplyResources(this.lbNom, "lbNom");
            this.lbNom.Name = "lbNom";
            // 
            // lbListeAdherent
            // 
            resources.ApplyResources(this.lbListeAdherent, "lbListeAdherent");
            this.lbListeAdherent.Name = "lbListeAdherent";
            // 
            // grdAdherent
            // 
            this.grdAdherent.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.grdAdherent.AutoArrange = false;
            this.grdAdherent.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colHeaderID,
            this.colHeaderNom,
            this.colHeaderPrenom,
            this.colHeaderSexe,
            this.colHeaderDateNaiss,
            this.colHeaderAdresse,
            this.colHeaderVille,
            this.colHeaderCP});
            resources.ApplyResources(this.grdAdherent, "grdAdherent");
            this.grdAdherent.FullRowSelect = true;
            this.grdAdherent.GridLines = true;
            this.grdAdherent.HideSelection = false;
            this.grdAdherent.MultiSelect = false;
            this.grdAdherent.Name = "grdAdherent";
            this.grdAdherent.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdAdherent.UseCompatibleStateImageBehavior = false;
            this.grdAdherent.View = System.Windows.Forms.View.Details;
            this.grdAdherent.SelectedIndexChanged += new System.EventHandler(this.grdAdherent_SelectedIndexChanged);
            // 
            // colHeaderID
            // 
            resources.ApplyResources(this.colHeaderID, "colHeaderID");
            // 
            // colHeaderNom
            // 
            resources.ApplyResources(this.colHeaderNom, "colHeaderNom");
            // 
            // colHeaderPrenom
            // 
            resources.ApplyResources(this.colHeaderPrenom, "colHeaderPrenom");
            // 
            // colHeaderSexe
            // 
            resources.ApplyResources(this.colHeaderSexe, "colHeaderSexe");
            // 
            // colHeaderDateNaiss
            // 
            resources.ApplyResources(this.colHeaderDateNaiss, "colHeaderDateNaiss");
            // 
            // colHeaderAdresse
            // 
            resources.ApplyResources(this.colHeaderAdresse, "colHeaderAdresse");
            // 
            // colHeaderVille
            // 
            resources.ApplyResources(this.colHeaderVille, "colHeaderVille");
            // 
            // colHeaderCP
            // 
            resources.ApplyResources(this.colHeaderCP, "colHeaderCP");
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnEdit, "btnEdit");
            this.btnEdit.ForeColor = System.Drawing.Color.White;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnAdd, "btnAdd");
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDel
            // 
            this.btnDel.BackColor = System.Drawing.Color.Crimson;
            this.btnDel.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnDel, "btnDel");
            this.btnDel.ForeColor = System.Drawing.Color.White;
            this.btnDel.Name = "btnDel";
            this.btnDel.UseVisualStyleBackColor = false;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // tabPageAuteurEditeur
            // 
            this.tabPageAuteurEditeur.Controls.Add(this.lbListeEditeurs);
            this.tabPageAuteurEditeur.Controls.Add(this.lbListeAuteurs);
            this.tabPageAuteurEditeur.Controls.Add(this.btnEditEditeur);
            this.tabPageAuteurEditeur.Controls.Add(this.btnAddEditeur);
            this.tabPageAuteurEditeur.Controls.Add(this.btnDelEditeur);
            this.tabPageAuteurEditeur.Controls.Add(this.grdEditeur);
            this.tabPageAuteurEditeur.Controls.Add(this.grpDetailsEditeur);
            this.tabPageAuteurEditeur.Controls.Add(this.btnEditAuteur);
            this.tabPageAuteurEditeur.Controls.Add(this.btnAddAuteur);
            this.tabPageAuteurEditeur.Controls.Add(this.btnDelAuteur);
            this.tabPageAuteurEditeur.Controls.Add(this.grdAuteur);
            this.tabPageAuteurEditeur.Controls.Add(this.grpDetailsAuteur);
            resources.ApplyResources(this.tabPageAuteurEditeur, "tabPageAuteurEditeur");
            this.tabPageAuteurEditeur.Name = "tabPageAuteurEditeur";
            this.tabPageAuteurEditeur.UseVisualStyleBackColor = true;
            // 
            // lbListeEditeurs
            // 
            resources.ApplyResources(this.lbListeEditeurs, "lbListeEditeurs");
            this.lbListeEditeurs.Name = "lbListeEditeurs";
            // 
            // lbListeAuteurs
            // 
            resources.ApplyResources(this.lbListeAuteurs, "lbListeAuteurs");
            this.lbListeAuteurs.Name = "lbListeAuteurs";
            // 
            // btnEditEditeur
            // 
            this.btnEditEditeur.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnEditEditeur.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnEditEditeur, "btnEditEditeur");
            this.btnEditEditeur.ForeColor = System.Drawing.Color.White;
            this.btnEditEditeur.Name = "btnEditEditeur";
            this.btnEditEditeur.UseVisualStyleBackColor = false;
            this.btnEditEditeur.Click += new System.EventHandler(this.btnEditEditeur_Click);
            // 
            // btnAddEditeur
            // 
            this.btnAddEditeur.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnAddEditeur.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnAddEditeur, "btnAddEditeur");
            this.btnAddEditeur.ForeColor = System.Drawing.Color.White;
            this.btnAddEditeur.Name = "btnAddEditeur";
            this.btnAddEditeur.UseVisualStyleBackColor = false;
            this.btnAddEditeur.Click += new System.EventHandler(this.btnAddEditeur_Click);
            // 
            // btnDelEditeur
            // 
            this.btnDelEditeur.BackColor = System.Drawing.Color.Crimson;
            this.btnDelEditeur.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnDelEditeur, "btnDelEditeur");
            this.btnDelEditeur.ForeColor = System.Drawing.Color.White;
            this.btnDelEditeur.Name = "btnDelEditeur";
            this.btnDelEditeur.UseVisualStyleBackColor = false;
            this.btnDelEditeur.Click += new System.EventHandler(this.btnDelEditeur_Click);
            // 
            // grdEditeur
            // 
            this.grdEditeur.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.grdEditeur.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colHeaderEditeursID,
            this.colHeaderEditeursNom,
            this.colHeaderEditeursPrenom});
            this.grdEditeur.FullRowSelect = true;
            this.grdEditeur.GridLines = true;
            this.grdEditeur.HideSelection = false;
            resources.ApplyResources(this.grdEditeur, "grdEditeur");
            this.grdEditeur.MultiSelect = false;
            this.grdEditeur.Name = "grdEditeur";
            this.grdEditeur.UseCompatibleStateImageBehavior = false;
            this.grdEditeur.View = System.Windows.Forms.View.Details;
            this.grdEditeur.SelectedIndexChanged += new System.EventHandler(this.grdEditeur_SelectedIndexChanged);
            // 
            // colHeaderEditeursID
            // 
            resources.ApplyResources(this.colHeaderEditeursID, "colHeaderEditeursID");
            // 
            // colHeaderEditeursNom
            // 
            resources.ApplyResources(this.colHeaderEditeursNom, "colHeaderEditeursNom");
            // 
            // colHeaderEditeursPrenom
            // 
            resources.ApplyResources(this.colHeaderEditeursPrenom, "colHeaderEditeursPrenom");
            // 
            // grpDetailsEditeur
            // 
            this.grpDetailsEditeur.Controls.Add(this.txtPrenomEditeur);
            this.grpDetailsEditeur.Controls.Add(this.lbPrenomEditeur);
            this.grpDetailsEditeur.Controls.Add(this.txtNomEditeur);
            this.grpDetailsEditeur.Controls.Add(this.lbNomEditeur);
            this.grpDetailsEditeur.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            resources.ApplyResources(this.grpDetailsEditeur, "grpDetailsEditeur");
            this.grpDetailsEditeur.Name = "grpDetailsEditeur";
            this.grpDetailsEditeur.TabStop = false;
            // 
            // txtPrenomEditeur
            // 
            resources.ApplyResources(this.txtPrenomEditeur, "txtPrenomEditeur");
            this.txtPrenomEditeur.Name = "txtPrenomEditeur";
            // 
            // lbPrenomEditeur
            // 
            resources.ApplyResources(this.lbPrenomEditeur, "lbPrenomEditeur");
            this.lbPrenomEditeur.Name = "lbPrenomEditeur";
            // 
            // txtNomEditeur
            // 
            resources.ApplyResources(this.txtNomEditeur, "txtNomEditeur");
            this.txtNomEditeur.Name = "txtNomEditeur";
            // 
            // lbNomEditeur
            // 
            resources.ApplyResources(this.lbNomEditeur, "lbNomEditeur");
            this.lbNomEditeur.Name = "lbNomEditeur";
            // 
            // btnEditAuteur
            // 
            this.btnEditAuteur.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnEditAuteur.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnEditAuteur, "btnEditAuteur");
            this.btnEditAuteur.ForeColor = System.Drawing.Color.White;
            this.btnEditAuteur.Name = "btnEditAuteur";
            this.btnEditAuteur.UseVisualStyleBackColor = false;
            this.btnEditAuteur.Click += new System.EventHandler(this.btnEditAuteur_Click);
            // 
            // btnAddAuteur
            // 
            this.btnAddAuteur.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnAddAuteur.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnAddAuteur, "btnAddAuteur");
            this.btnAddAuteur.ForeColor = System.Drawing.Color.White;
            this.btnAddAuteur.Name = "btnAddAuteur";
            this.btnAddAuteur.UseVisualStyleBackColor = false;
            this.btnAddAuteur.Click += new System.EventHandler(this.btnAddAuteur_Click);
            // 
            // btnDelAuteur
            // 
            this.btnDelAuteur.BackColor = System.Drawing.Color.Crimson;
            this.btnDelAuteur.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnDelAuteur, "btnDelAuteur");
            this.btnDelAuteur.ForeColor = System.Drawing.Color.White;
            this.btnDelAuteur.Name = "btnDelAuteur";
            this.btnDelAuteur.UseVisualStyleBackColor = false;
            this.btnDelAuteur.Click += new System.EventHandler(this.btnDelAuteur_Click);
            // 
            // grdAuteur
            // 
            this.grdAuteur.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.grdAuteur.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colHeaderAuteursID,
            this.colHeaderAuteursNom,
            this.colHeaderAuteursPrenom});
            this.grdAuteur.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.grdAuteur.FullRowSelect = true;
            this.grdAuteur.GridLines = true;
            this.grdAuteur.HideSelection = false;
            resources.ApplyResources(this.grdAuteur, "grdAuteur");
            this.grdAuteur.MultiSelect = false;
            this.grdAuteur.Name = "grdAuteur";
            this.grdAuteur.UseCompatibleStateImageBehavior = false;
            this.grdAuteur.View = System.Windows.Forms.View.Details;
            this.grdAuteur.SelectedIndexChanged += new System.EventHandler(this.grdAuteur_SelectedIndexChanged);
            // 
            // colHeaderAuteursID
            // 
            resources.ApplyResources(this.colHeaderAuteursID, "colHeaderAuteursID");
            // 
            // colHeaderAuteursNom
            // 
            resources.ApplyResources(this.colHeaderAuteursNom, "colHeaderAuteursNom");
            // 
            // colHeaderAuteursPrenom
            // 
            resources.ApplyResources(this.colHeaderAuteursPrenom, "colHeaderAuteursPrenom");
            // 
            // grpDetailsAuteur
            // 
            this.grpDetailsAuteur.Controls.Add(this.txtPrenomAuteur);
            this.grpDetailsAuteur.Controls.Add(this.lbPrenomAuteur);
            this.grpDetailsAuteur.Controls.Add(this.txtNomAuteur);
            this.grpDetailsAuteur.Controls.Add(this.lbNomAuteur);
            this.grpDetailsAuteur.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            resources.ApplyResources(this.grpDetailsAuteur, "grpDetailsAuteur");
            this.grpDetailsAuteur.Name = "grpDetailsAuteur";
            this.grpDetailsAuteur.TabStop = false;
            // 
            // txtPrenomAuteur
            // 
            resources.ApplyResources(this.txtPrenomAuteur, "txtPrenomAuteur");
            this.txtPrenomAuteur.Name = "txtPrenomAuteur";
            // 
            // lbPrenomAuteur
            // 
            resources.ApplyResources(this.lbPrenomAuteur, "lbPrenomAuteur");
            this.lbPrenomAuteur.Name = "lbPrenomAuteur";
            // 
            // txtNomAuteur
            // 
            resources.ApplyResources(this.txtNomAuteur, "txtNomAuteur");
            this.txtNomAuteur.Name = "txtNomAuteur";
            // 
            // lbNomAuteur
            // 
            resources.ApplyResources(this.lbNomAuteur, "lbNomAuteur");
            this.lbNomAuteur.Name = "lbNomAuteur";
            // 
            // tabPageLivre
            // 
            this.tabPageLivre.Controls.Add(this.lbListeLivres);
            this.tabPageLivre.Controls.Add(this.grdLivre);
            this.tabPageLivre.Controls.Add(this.btnEditLivre);
            this.tabPageLivre.Controls.Add(this.grpDetailsLivre);
            this.tabPageLivre.Controls.Add(this.btnAddLivre);
            this.tabPageLivre.Controls.Add(this.btnDelLivre);
            resources.ApplyResources(this.tabPageLivre, "tabPageLivre");
            this.tabPageLivre.Name = "tabPageLivre";
            this.tabPageLivre.UseVisualStyleBackColor = true;
            // 
            // lbListeLivres
            // 
            resources.ApplyResources(this.lbListeLivres, "lbListeLivres");
            this.lbListeLivres.Name = "lbListeLivres";
            // 
            // grdLivre
            // 
            this.grdLivre.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.grdLivre.AutoArrange = false;
            this.grdLivre.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colHeaderLivresID,
            this.colHeaderLivresTitre,
            this.colHeaderLivresISBN,
            this.colHeaderLivresAuteur,
            this.colHeaderLivresEditeur,
            this.colHeaderLivresDateE,
            this.colHeaderLivreNbP,
            this.colHeaderLivreQte});
            resources.ApplyResources(this.grdLivre, "grdLivre");
            this.grdLivre.FullRowSelect = true;
            this.grdLivre.GridLines = true;
            this.grdLivre.HideSelection = false;
            this.grdLivre.MultiSelect = false;
            this.grdLivre.Name = "grdLivre";
            this.grdLivre.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdLivre.UseCompatibleStateImageBehavior = false;
            this.grdLivre.View = System.Windows.Forms.View.Details;
            this.grdLivre.SelectedIndexChanged += new System.EventHandler(this.grdLivre_SelectedIndexChanged);
            // 
            // colHeaderLivresID
            // 
            resources.ApplyResources(this.colHeaderLivresID, "colHeaderLivresID");
            // 
            // colHeaderLivresTitre
            // 
            resources.ApplyResources(this.colHeaderLivresTitre, "colHeaderLivresTitre");
            // 
            // colHeaderLivresISBN
            // 
            resources.ApplyResources(this.colHeaderLivresISBN, "colHeaderLivresISBN");
            // 
            // colHeaderLivresAuteur
            // 
            resources.ApplyResources(this.colHeaderLivresAuteur, "colHeaderLivresAuteur");
            // 
            // colHeaderLivresEditeur
            // 
            resources.ApplyResources(this.colHeaderLivresEditeur, "colHeaderLivresEditeur");
            // 
            // colHeaderLivresDateE
            // 
            resources.ApplyResources(this.colHeaderLivresDateE, "colHeaderLivresDateE");
            // 
            // colHeaderLivreNbP
            // 
            resources.ApplyResources(this.colHeaderLivreNbP, "colHeaderLivreNbP");
            // 
            // colHeaderLivreQte
            // 
            resources.ApplyResources(this.colHeaderLivreQte, "colHeaderLivreQte");
            // 
            // btnEditLivre
            // 
            this.btnEditLivre.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnEditLivre.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnEditLivre, "btnEditLivre");
            this.btnEditLivre.ForeColor = System.Drawing.Color.White;
            this.btnEditLivre.Name = "btnEditLivre";
            this.btnEditLivre.UseVisualStyleBackColor = false;
            this.btnEditLivre.Click += new System.EventHandler(this.btnEditLivre_Click);
            // 
            // grpDetailsLivre
            // 
            this.grpDetailsLivre.Controls.Add(this.dateTPickerLivre);
            this.grpDetailsLivre.Controls.Add(this.dropDownEditeur);
            this.grpDetailsLivre.Controls.Add(this.lbEditeur);
            this.grpDetailsLivre.Controls.Add(this.txtNbPage);
            this.grpDetailsLivre.Controls.Add(this.lbNbPages);
            this.grpDetailsLivre.Controls.Add(this.dropDownAuteur);
            this.grpDetailsLivre.Controls.Add(this.txtDateEdition);
            this.grpDetailsLivre.Controls.Add(this.lbDateEdition);
            this.grpDetailsLivre.Controls.Add(this.txtISBN);
            this.grpDetailsLivre.Controls.Add(this.lbISBN);
            this.grpDetailsLivre.Controls.Add(this.lbAuteur);
            this.grpDetailsLivre.Controls.Add(this.txtTitre);
            this.grpDetailsLivre.Controls.Add(this.lbTitre);
            this.grpDetailsLivre.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            resources.ApplyResources(this.grpDetailsLivre, "grpDetailsLivre");
            this.grpDetailsLivre.Name = "grpDetailsLivre";
            this.grpDetailsLivre.TabStop = false;
            // 
            // dateTPickerLivre
            // 
            this.dateTPickerLivre.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.dateTPickerLivre, "dateTPickerLivre");
            this.dateTPickerLivre.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTPickerLivre.Name = "dateTPickerLivre";
            this.dateTPickerLivre.Value = new System.DateTime(2014, 5, 2, 0, 0, 0, 0);
            this.dateTPickerLivre.ValueChanged += new System.EventHandler(this.dateTPickerLivre_ValueChanged);
            // 
            // dropDownEditeur
            // 
            this.dropDownEditeur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropDownEditeur.FormattingEnabled = true;
            resources.ApplyResources(this.dropDownEditeur, "dropDownEditeur");
            this.dropDownEditeur.Name = "dropDownEditeur";
            this.dropDownEditeur.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dropDownEditeur_MouseClick);
            // 
            // lbEditeur
            // 
            resources.ApplyResources(this.lbEditeur, "lbEditeur");
            this.lbEditeur.Name = "lbEditeur";
            // 
            // txtNbPage
            // 
            resources.ApplyResources(this.txtNbPage, "txtNbPage");
            this.txtNbPage.Name = "txtNbPage";
            // 
            // lbNbPages
            // 
            resources.ApplyResources(this.lbNbPages, "lbNbPages");
            this.lbNbPages.Name = "lbNbPages";
            // 
            // dropDownAuteur
            // 
            this.dropDownAuteur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropDownAuteur.FormattingEnabled = true;
            resources.ApplyResources(this.dropDownAuteur, "dropDownAuteur");
            this.dropDownAuteur.Name = "dropDownAuteur";
            this.dropDownAuteur.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dropDownAuteur_MouseClick);
            // 
            // txtDateEdition
            // 
            resources.ApplyResources(this.txtDateEdition, "txtDateEdition");
            this.txtDateEdition.Name = "txtDateEdition";
            // 
            // lbDateEdition
            // 
            resources.ApplyResources(this.lbDateEdition, "lbDateEdition");
            this.lbDateEdition.Name = "lbDateEdition";
            // 
            // txtISBN
            // 
            resources.ApplyResources(this.txtISBN, "txtISBN");
            this.txtISBN.Name = "txtISBN";
            // 
            // lbISBN
            // 
            resources.ApplyResources(this.lbISBN, "lbISBN");
            this.lbISBN.Name = "lbISBN";
            // 
            // lbAuteur
            // 
            resources.ApplyResources(this.lbAuteur, "lbAuteur");
            this.lbAuteur.Name = "lbAuteur";
            // 
            // txtTitre
            // 
            resources.ApplyResources(this.txtTitre, "txtTitre");
            this.txtTitre.Name = "txtTitre";
            // 
            // lbTitre
            // 
            resources.ApplyResources(this.lbTitre, "lbTitre");
            this.lbTitre.Name = "lbTitre";
            // 
            // btnAddLivre
            // 
            this.btnAddLivre.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnAddLivre.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnAddLivre, "btnAddLivre");
            this.btnAddLivre.ForeColor = System.Drawing.Color.White;
            this.btnAddLivre.Name = "btnAddLivre";
            this.btnAddLivre.UseVisualStyleBackColor = false;
            this.btnAddLivre.Click += new System.EventHandler(this.btnAddLivre_Click);
            // 
            // btnDelLivre
            // 
            this.btnDelLivre.BackColor = System.Drawing.Color.Crimson;
            this.btnDelLivre.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnDelLivre, "btnDelLivre");
            this.btnDelLivre.ForeColor = System.Drawing.Color.White;
            this.btnDelLivre.Name = "btnDelLivre";
            this.btnDelLivre.UseVisualStyleBackColor = false;
            this.btnDelLivre.Click += new System.EventHandler(this.btnDelLivre_Click);
            // 
            // tabPageExemplaire
            // 
            this.tabPageExemplaire.Controls.Add(this.lbListeExemplaires);
            this.tabPageExemplaire.Controls.Add(this.grdExemplaire);
            this.tabPageExemplaire.Controls.Add(this.btnEditExemplaire);
            this.tabPageExemplaire.Controls.Add(this.grpDetailsExemplaire);
            this.tabPageExemplaire.Controls.Add(this.btnAddExemplaire);
            this.tabPageExemplaire.Controls.Add(this.btnDelExemplaire);
            resources.ApplyResources(this.tabPageExemplaire, "tabPageExemplaire");
            this.tabPageExemplaire.Name = "tabPageExemplaire";
            this.tabPageExemplaire.UseVisualStyleBackColor = true;
            // 
            // lbListeExemplaires
            // 
            resources.ApplyResources(this.lbListeExemplaires, "lbListeExemplaires");
            this.lbListeExemplaires.Name = "lbListeExemplaires";
            // 
            // grdExemplaire
            // 
            this.grdExemplaire.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.grdExemplaire.AutoArrange = false;
            this.grdExemplaire.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colHeaderExemplaireID,
            this.colHeaderExemplaireLivreID,
            this.colHeaderExemplaireTitre,
            this.colHeaderExemplaireEtat,
            this.colHeaderExemplaireDateAcquis});
            resources.ApplyResources(this.grdExemplaire, "grdExemplaire");
            this.grdExemplaire.FullRowSelect = true;
            this.grdExemplaire.GridLines = true;
            this.grdExemplaire.HideSelection = false;
            this.grdExemplaire.MultiSelect = false;
            this.grdExemplaire.Name = "grdExemplaire";
            this.grdExemplaire.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdExemplaire.UseCompatibleStateImageBehavior = false;
            this.grdExemplaire.View = System.Windows.Forms.View.Details;
            this.grdExemplaire.SelectedIndexChanged += new System.EventHandler(this.grdExemplaire_SelectedIndexChanged);
            // 
            // colHeaderExemplaireID
            // 
            resources.ApplyResources(this.colHeaderExemplaireID, "colHeaderExemplaireID");
            // 
            // colHeaderExemplaireLivreID
            // 
            resources.ApplyResources(this.colHeaderExemplaireLivreID, "colHeaderExemplaireLivreID");
            // 
            // colHeaderExemplaireTitre
            // 
            resources.ApplyResources(this.colHeaderExemplaireTitre, "colHeaderExemplaireTitre");
            // 
            // colHeaderExemplaireEtat
            // 
            resources.ApplyResources(this.colHeaderExemplaireEtat, "colHeaderExemplaireEtat");
            // 
            // colHeaderExemplaireDateAcquis
            // 
            resources.ApplyResources(this.colHeaderExemplaireDateAcquis, "colHeaderExemplaireDateAcquis");
            // 
            // btnEditExemplaire
            // 
            this.btnEditExemplaire.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnEditExemplaire.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnEditExemplaire, "btnEditExemplaire");
            this.btnEditExemplaire.ForeColor = System.Drawing.Color.White;
            this.btnEditExemplaire.Name = "btnEditExemplaire";
            this.btnEditExemplaire.UseVisualStyleBackColor = false;
            this.btnEditExemplaire.Click += new System.EventHandler(this.btnEditExemplaire_Click);
            // 
            // grpDetailsExemplaire
            // 
            this.grpDetailsExemplaire.Controls.Add(this.dateTPickerExemplaire);
            this.grpDetailsExemplaire.Controls.Add(this.dropDownEtat);
            this.grpDetailsExemplaire.Controls.Add(this.lbEtat);
            this.grpDetailsExemplaire.Controls.Add(this.dropDownLivre);
            this.grpDetailsExemplaire.Controls.Add(this.lbEtat2);
            this.grpDetailsExemplaire.Controls.Add(this.txtDateAcquis);
            this.grpDetailsExemplaire.Controls.Add(this.lbDateAcquis);
            this.grpDetailsExemplaire.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            resources.ApplyResources(this.grpDetailsExemplaire, "grpDetailsExemplaire");
            this.grpDetailsExemplaire.Name = "grpDetailsExemplaire";
            this.grpDetailsExemplaire.TabStop = false;
            // 
            // dateTPickerExemplaire
            // 
            this.dateTPickerExemplaire.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.dateTPickerExemplaire, "dateTPickerExemplaire");
            this.dateTPickerExemplaire.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTPickerExemplaire.Name = "dateTPickerExemplaire";
            this.dateTPickerExemplaire.Value = new System.DateTime(2014, 5, 2, 0, 0, 0, 0);
            this.dateTPickerExemplaire.ValueChanged += new System.EventHandler(this.dateTPickerExemplaire_ValueChanged);
            // 
            // dropDownEtat
            // 
            this.dropDownEtat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropDownEtat.FormattingEnabled = true;
            this.dropDownEtat.Items.AddRange(new object[] {
            resources.GetString("dropDownEtat.Items"),
            resources.GetString("dropDownEtat.Items1"),
            resources.GetString("dropDownEtat.Items2"),
            resources.GetString("dropDownEtat.Items3"),
            resources.GetString("dropDownEtat.Items4"),
            resources.GetString("dropDownEtat.Items5")});
            resources.ApplyResources(this.dropDownEtat, "dropDownEtat");
            this.dropDownEtat.Name = "dropDownEtat";
            // 
            // lbEtat
            // 
            resources.ApplyResources(this.lbEtat, "lbEtat");
            this.lbEtat.Name = "lbEtat";
            // 
            // dropDownLivre
            // 
            this.dropDownLivre.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropDownLivre.FormattingEnabled = true;
            resources.ApplyResources(this.dropDownLivre, "dropDownLivre");
            this.dropDownLivre.Name = "dropDownLivre";
            this.dropDownLivre.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dropDownLivre_MouseClick);
            // 
            // lbEtat2
            // 
            resources.ApplyResources(this.lbEtat2, "lbEtat2");
            this.lbEtat2.Name = "lbEtat2";
            // 
            // txtDateAcquis
            // 
            resources.ApplyResources(this.txtDateAcquis, "txtDateAcquis");
            this.txtDateAcquis.Name = "txtDateAcquis";
            // 
            // lbDateAcquis
            // 
            resources.ApplyResources(this.lbDateAcquis, "lbDateAcquis");
            this.lbDateAcquis.Name = "lbDateAcquis";
            // 
            // btnAddExemplaire
            // 
            this.btnAddExemplaire.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnAddExemplaire.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnAddExemplaire, "btnAddExemplaire");
            this.btnAddExemplaire.ForeColor = System.Drawing.Color.White;
            this.btnAddExemplaire.Name = "btnAddExemplaire";
            this.btnAddExemplaire.UseVisualStyleBackColor = false;
            this.btnAddExemplaire.Click += new System.EventHandler(this.btnAddExemplaire_Click);
            // 
            // btnDelExemplaire
            // 
            this.btnDelExemplaire.BackColor = System.Drawing.Color.Crimson;
            this.btnDelExemplaire.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnDelExemplaire, "btnDelExemplaire");
            this.btnDelExemplaire.ForeColor = System.Drawing.Color.White;
            this.btnDelExemplaire.Name = "btnDelExemplaire";
            this.btnDelExemplaire.UseVisualStyleBackColor = false;
            this.btnDelExemplaire.Click += new System.EventHandler(this.btnDelExemplaire_Click);
            // 
            // menuStripMain
            // 
            this.menuStripMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripGestionEmprunts,
            this.toolStripAdministration});
            resources.ApplyResources(this.menuStripMain, "menuStripMain");
            this.menuStripMain.Name = "menuStripMain";
            // 
            // toolStripGestionEmprunts
            // 
            this.toolStripGestionEmprunts.Name = "toolStripGestionEmprunts";
            resources.ApplyResources(this.toolStripGestionEmprunts, "toolStripGestionEmprunts");
            this.toolStripGestionEmprunts.Click += new System.EventHandler(this.toolStripGestionEmprunts_Click);
            // 
            // toolStripAdministration
            // 
            this.toolStripAdministration.Name = "toolStripAdministration";
            resources.ApplyResources(this.toolStripAdministration, "toolStripAdministration");
            this.toolStripAdministration.Click += new System.EventHandler(this.toolStripAdministration_Click);
            // 
            // tabControlGestion
            // 
            this.tabControlGestion.Controls.Add(this.tabPageRetour);
            this.tabControlGestion.Controls.Add(this.tabPageEmprunt);
            resources.ApplyResources(this.tabControlGestion, "tabControlGestion");
            this.tabControlGestion.Name = "tabControlGestion";
            this.tabControlGestion.SelectedIndex = 0;
            // 
            // tabPageRetour
            // 
            this.tabPageRetour.Controls.Add(this.btnRestituer);
            this.tabPageRetour.Controls.Add(this.grpDetailsEmprunt);
            this.tabPageRetour.Controls.Add(this.lbEmprunt);
            this.tabPageRetour.Controls.Add(this.grdEmprunt);
            resources.ApplyResources(this.tabPageRetour, "tabPageRetour");
            this.tabPageRetour.Name = "tabPageRetour";
            this.tabPageRetour.UseVisualStyleBackColor = true;
            // 
            // btnRestituer
            // 
            this.btnRestituer.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnRestituer.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnRestituer, "btnRestituer");
            this.btnRestituer.ForeColor = System.Drawing.Color.White;
            this.btnRestituer.Name = "btnRestituer";
            this.btnRestituer.UseVisualStyleBackColor = false;
            this.btnRestituer.Click += new System.EventHandler(this.btnRestituer_Click);
            // 
            // grpDetailsEmprunt
            // 
            this.grpDetailsEmprunt.Controls.Add(this.grdListEmprunt);
            resources.ApplyResources(this.grpDetailsEmprunt, "grpDetailsEmprunt");
            this.grpDetailsEmprunt.Name = "grpDetailsEmprunt";
            this.grpDetailsEmprunt.TabStop = false;
            // 
            // grdListEmprunt
            // 
            this.grdListEmprunt.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.grdListEmprunt.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colHeaderEmprId,
            this.colHeaderEmprISBN,
            this.colHeaderEmprDEmpr,
            this.colHeaderEmprDRetour});
            this.grdListEmprunt.Cursor = System.Windows.Forms.Cursors.Default;
            resources.ApplyResources(this.grdListEmprunt, "grdListEmprunt");
            this.grdListEmprunt.FullRowSelect = true;
            this.grdListEmprunt.GridLines = true;
            this.grdListEmprunt.HideSelection = false;
            this.grdListEmprunt.MultiSelect = false;
            this.grdListEmprunt.Name = "grdListEmprunt";
            this.grdListEmprunt.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdListEmprunt.UseCompatibleStateImageBehavior = false;
            this.grdListEmprunt.View = System.Windows.Forms.View.Details;
            this.grdListEmprunt.SelectedIndexChanged += new System.EventHandler(this.grdListEmprunt_SelectedIndexChanged);
            // 
            // colHeaderEmprId
            // 
            resources.ApplyResources(this.colHeaderEmprId, "colHeaderEmprId");
            // 
            // colHeaderEmprISBN
            // 
            resources.ApplyResources(this.colHeaderEmprISBN, "colHeaderEmprISBN");
            // 
            // colHeaderEmprDEmpr
            // 
            resources.ApplyResources(this.colHeaderEmprDEmpr, "colHeaderEmprDEmpr");
            // 
            // colHeaderEmprDRetour
            // 
            resources.ApplyResources(this.colHeaderEmprDRetour, "colHeaderEmprDRetour");
            // 
            // lbEmprunt
            // 
            resources.ApplyResources(this.lbEmprunt, "lbEmprunt");
            this.lbEmprunt.Name = "lbEmprunt";
            // 
            // grdEmprunt
            // 
            this.grdEmprunt.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.grdEmprunt.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ccolHeaderEmprID,
            this.colHeaderEmprNom,
            this.colHeaderEmprPrenom,
            this.colHeaderEmprQte});
            this.grdEmprunt.Cursor = System.Windows.Forms.Cursors.Default;
            this.grdEmprunt.FullRowSelect = true;
            this.grdEmprunt.GridLines = true;
            this.grdEmprunt.HideSelection = false;
            resources.ApplyResources(this.grdEmprunt, "grdEmprunt");
            this.grdEmprunt.MultiSelect = false;
            this.grdEmprunt.Name = "grdEmprunt";
            this.grdEmprunt.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdEmprunt.UseCompatibleStateImageBehavior = false;
            this.grdEmprunt.View = System.Windows.Forms.View.Details;
            this.grdEmprunt.SelectedIndexChanged += new System.EventHandler(this.grdEmprunt_SelectedIndexChanged);
            // 
            // ccolHeaderEmprID
            // 
            resources.ApplyResources(this.ccolHeaderEmprID, "ccolHeaderEmprID");
            // 
            // colHeaderEmprNom
            // 
            resources.ApplyResources(this.colHeaderEmprNom, "colHeaderEmprNom");
            // 
            // colHeaderEmprPrenom
            // 
            resources.ApplyResources(this.colHeaderEmprPrenom, "colHeaderEmprPrenom");
            // 
            // colHeaderEmprQte
            // 
            resources.ApplyResources(this.colHeaderEmprQte, "colHeaderEmprQte");
            // 
            // tabPageEmprunt
            // 
            this.tabPageEmprunt.Controls.Add(this.label5);
            this.tabPageEmprunt.Controls.Add(this.lbidEPret);
            this.tabPageEmprunt.Controls.Add(this.lbidLPret);
            this.tabPageEmprunt.Controls.Add(this.lbidAPret);
            this.tabPageEmprunt.Controls.Add(this.txtExemplaire);
            this.tabPageEmprunt.Controls.Add(this.dropDownExemplaire);
            this.tabPageEmprunt.Controls.Add(this.label4);
            this.tabPageEmprunt.Controls.Add(this.btnPret);
            this.tabPageEmprunt.Controls.Add(this.dateTPickerPret);
            this.tabPageEmprunt.Controls.Add(this.txtDatePret);
            this.tabPageEmprunt.Controls.Add(this.label3);
            this.tabPageEmprunt.Controls.Add(this.txtLivre);
            this.tabPageEmprunt.Controls.Add(this.txtAdherent);
            this.tabPageEmprunt.Controls.Add(this.dropDownLivrePret);
            this.tabPageEmprunt.Controls.Add(this.label1);
            this.tabPageEmprunt.Controls.Add(this.dropDownAdherent);
            this.tabPageEmprunt.Controls.Add(this.label2);
            this.tabPageEmprunt.Controls.Add(this.grpDetailsPretExemplaire);
            resources.ApplyResources(this.tabPageEmprunt, "tabPageEmprunt");
            this.tabPageEmprunt.Name = "tabPageEmprunt";
            this.tabPageEmprunt.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // lbidEPret
            // 
            resources.ApplyResources(this.lbidEPret, "lbidEPret");
            this.lbidEPret.Name = "lbidEPret";
            // 
            // lbidLPret
            // 
            resources.ApplyResources(this.lbidLPret, "lbidLPret");
            this.lbidLPret.Name = "lbidLPret";
            // 
            // lbidAPret
            // 
            resources.ApplyResources(this.lbidAPret, "lbidAPret");
            this.lbidAPret.Name = "lbidAPret";
            // 
            // txtExemplaire
            // 
            resources.ApplyResources(this.txtExemplaire, "txtExemplaire");
            this.txtExemplaire.Name = "txtExemplaire";
            // 
            // dropDownExemplaire
            // 
            this.dropDownExemplaire.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropDownExemplaire.FormattingEnabled = true;
            resources.ApplyResources(this.dropDownExemplaire, "dropDownExemplaire");
            this.dropDownExemplaire.Name = "dropDownExemplaire";
            this.dropDownExemplaire.TextChanged += new System.EventHandler(this.dropDownExemplaire_TextChanged);
            this.dropDownExemplaire.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dropDownExemplaire_MouseClick);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // btnPret
            // 
            this.btnPret.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnPret.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnPret, "btnPret");
            this.btnPret.ForeColor = System.Drawing.Color.White;
            this.btnPret.Name = "btnPret";
            this.btnPret.UseVisualStyleBackColor = false;
            this.btnPret.Click += new System.EventHandler(this.btnPret_Click);
            // 
            // dateTPickerPret
            // 
            this.dateTPickerPret.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.dateTPickerPret, "dateTPickerPret");
            this.dateTPickerPret.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTPickerPret.Name = "dateTPickerPret";
            this.dateTPickerPret.Value = new System.DateTime(2014, 5, 2, 0, 0, 0, 0);
            this.dateTPickerPret.ValueChanged += new System.EventHandler(this.dateTPickerPret_ValueChanged);
            // 
            // txtDatePret
            // 
            resources.ApplyResources(this.txtDatePret, "txtDatePret");
            this.txtDatePret.Name = "txtDatePret";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // txtLivre
            // 
            resources.ApplyResources(this.txtLivre, "txtLivre");
            this.txtLivre.Name = "txtLivre";
            // 
            // txtAdherent
            // 
            resources.ApplyResources(this.txtAdherent, "txtAdherent");
            this.txtAdherent.Name = "txtAdherent";
            // 
            // dropDownLivrePret
            // 
            this.dropDownLivrePret.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropDownLivrePret.FormattingEnabled = true;
            resources.ApplyResources(this.dropDownLivrePret, "dropDownLivrePret");
            this.dropDownLivrePret.Name = "dropDownLivrePret";
            this.dropDownLivrePret.TextChanged += new System.EventHandler(this.dropDownLivrePret_TextChanged);
            this.dropDownLivrePret.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dropDownLivrePret_MouseClick);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // dropDownAdherent
            // 
            this.dropDownAdherent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropDownAdherent.FormattingEnabled = true;
            resources.ApplyResources(this.dropDownAdherent, "dropDownAdherent");
            this.dropDownAdherent.Name = "dropDownAdherent";
            this.dropDownAdherent.TextChanged += new System.EventHandler(this.dropDownAdherent_TextChanged);
            this.dropDownAdherent.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dropDownAdherent_MouseClick);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // grpDetailsPretExemplaire
            // 
            this.grpDetailsPretExemplaire.Controls.Add(this.grdListExemplaires);
            resources.ApplyResources(this.grpDetailsPretExemplaire, "grpDetailsPretExemplaire");
            this.grpDetailsPretExemplaire.Name = "grpDetailsPretExemplaire";
            this.grpDetailsPretExemplaire.TabStop = false;
            // 
            // grdListExemplaires
            // 
            this.grdListExemplaires.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.grdListExemplaires.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colHeaderPretID,
            this.colHeaderPretEtat,
            this.colHeaderPretDRetour});
            this.grdListExemplaires.Cursor = System.Windows.Forms.Cursors.Default;
            resources.ApplyResources(this.grdListExemplaires, "grdListExemplaires");
            this.grdListExemplaires.FullRowSelect = true;
            this.grdListExemplaires.GridLines = true;
            this.grdListExemplaires.HideSelection = false;
            this.grdListExemplaires.MultiSelect = false;
            this.grdListExemplaires.Name = "grdListExemplaires";
            this.grdListExemplaires.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdListExemplaires.UseCompatibleStateImageBehavior = false;
            this.grdListExemplaires.View = System.Windows.Forms.View.Details;
            this.grdListExemplaires.SelectedIndexChanged += new System.EventHandler(this.grdListExemplaires_SelectedIndexChanged);
            // 
            // colHeaderPretID
            // 
            resources.ApplyResources(this.colHeaderPretID, "colHeaderPretID");
            // 
            // colHeaderPretEtat
            // 
            resources.ApplyResources(this.colHeaderPretEtat, "colHeaderPretEtat");
            // 
            // colHeaderPretDRetour
            // 
            resources.ApplyResources(this.colHeaderPretDRetour, "colHeaderPretDRetour");
            // 
            // Bibliotheque
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControlGestion);
            this.Controls.Add(this.menuStripMain);
            this.Controls.Add(this.tabControlAdmin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MainMenuStrip = this.menuStripMain;
            this.MaximizeBox = false;
            this.Name = "Bibliotheque";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Load += new System.EventHandler(this.Bibliotheque_Load);
            this.tabControlAdmin.ResumeLayout(false);
            this.tabPageAdherent.ResumeLayout(false);
            this.tabPageAdherent.PerformLayout();
            this.grpDetailsAdherent.ResumeLayout(false);
            this.grpDetailsAdherent.PerformLayout();
            this.grbSex.ResumeLayout(false);
            this.tabPageAuteurEditeur.ResumeLayout(false);
            this.grpDetailsEditeur.ResumeLayout(false);
            this.grpDetailsEditeur.PerformLayout();
            this.grpDetailsAuteur.ResumeLayout(false);
            this.grpDetailsAuteur.PerformLayout();
            this.tabPageLivre.ResumeLayout(false);
            this.tabPageLivre.PerformLayout();
            this.grpDetailsLivre.ResumeLayout(false);
            this.grpDetailsLivre.PerformLayout();
            this.tabPageExemplaire.ResumeLayout(false);
            this.tabPageExemplaire.PerformLayout();
            this.grpDetailsExemplaire.ResumeLayout(false);
            this.grpDetailsExemplaire.PerformLayout();
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.tabControlGestion.ResumeLayout(false);
            this.tabPageRetour.ResumeLayout(false);
            this.tabPageRetour.PerformLayout();
            this.grpDetailsEmprunt.ResumeLayout(false);
            this.tabPageEmprunt.ResumeLayout(false);
            this.tabPageEmprunt.PerformLayout();
            this.grpDetailsPretExemplaire.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlAdmin;
        private System.Windows.Forms.TabPage tabPageAdherent;
        internal System.Windows.Forms.ListView grdAdherent;
        internal System.Windows.Forms.Button btnEdit;
        internal System.Windows.Forms.GroupBox grpDetailsAdherent;
        private System.Windows.Forms.GroupBox grbSex;
        internal System.Windows.Forms.RadioButton rdFemme;
        internal System.Windows.Forms.RadioButton rdHomme;
        internal System.Windows.Forms.TextBox txtAdresse;
        internal System.Windows.Forms.Label lbAdresse;
        internal System.Windows.Forms.TextBox txtDateNaiss;
        internal System.Windows.Forms.Label lbDateNaiss;
        internal System.Windows.Forms.TextBox txtPrenom;
        internal System.Windows.Forms.Label lbPrenom;
        internal System.Windows.Forms.TextBox txtNom;
        internal System.Windows.Forms.Label lbNom;
        internal System.Windows.Forms.Button btnAdd;
        internal System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.TabPage tabPageLivre;
        private System.Windows.Forms.ColumnHeader colHeaderID;
        private System.Windows.Forms.ColumnHeader colHeaderNom;
        private System.Windows.Forms.ColumnHeader colHeaderPrenom;
        private System.Windows.Forms.ColumnHeader colHeaderDateNaiss;
        private System.Windows.Forms.ColumnHeader colHeaderAdresse;
        private System.Windows.Forms.ColumnHeader colHeaderVille;
        private System.Windows.Forms.ColumnHeader colHeaderCP;
        private System.Windows.Forms.TabPage tabPageAuteurEditeur;
        private System.Windows.Forms.TabPage tabPageExemplaire;
        internal System.Windows.Forms.TextBox txtCP;
        internal System.Windows.Forms.Label lbCP;
        internal System.Windows.Forms.TextBox txtVille;
        internal System.Windows.Forms.Label lbVille;
        private System.Windows.Forms.ColumnHeader colHeaderSexe;
        internal System.Windows.Forms.GroupBox grpDetailsAuteur;
        internal System.Windows.Forms.TextBox txtPrenomAuteur;
        internal System.Windows.Forms.Label lbPrenomAuteur;
        internal System.Windows.Forms.TextBox txtNomAuteur;
        internal System.Windows.Forms.Label lbNomAuteur;
        internal System.Windows.Forms.Button btnEditEditeur;
        internal System.Windows.Forms.Button btnAddEditeur;
        internal System.Windows.Forms.Button btnDelEditeur;
        private System.Windows.Forms.ListView grdEditeur;
        private System.Windows.Forms.ColumnHeader colHeaderEditeursID;
        private System.Windows.Forms.ColumnHeader colHeaderEditeursNom;
        private System.Windows.Forms.ColumnHeader colHeaderEditeursPrenom;
        internal System.Windows.Forms.GroupBox grpDetailsEditeur;
        internal System.Windows.Forms.TextBox txtPrenomEditeur;
        internal System.Windows.Forms.Label lbPrenomEditeur;
        internal System.Windows.Forms.TextBox txtNomEditeur;
        internal System.Windows.Forms.Label lbNomEditeur;
        internal System.Windows.Forms.Button btnEditAuteur;
        internal System.Windows.Forms.Button btnAddAuteur;
        internal System.Windows.Forms.Button btnDelAuteur;
        private System.Windows.Forms.ListView grdAuteur;
        private System.Windows.Forms.ColumnHeader colHeaderAuteursID;
        private System.Windows.Forms.ColumnHeader colHeaderAuteursNom;
        private System.Windows.Forms.ColumnHeader colHeaderAuteursPrenom;
        internal System.Windows.Forms.ListView grdLivre;
        private System.Windows.Forms.ColumnHeader colHeaderLivresID;
        private System.Windows.Forms.ColumnHeader colHeaderLivresTitre;
        private System.Windows.Forms.ColumnHeader colHeaderLivresISBN;
        private System.Windows.Forms.ColumnHeader colHeaderLivresAuteur;
        private System.Windows.Forms.ColumnHeader colHeaderLivresDateE;
        private System.Windows.Forms.ColumnHeader colHeaderLivreNbP;
        internal System.Windows.Forms.Button btnEditLivre;
        internal System.Windows.Forms.GroupBox grpDetailsLivre;
        internal System.Windows.Forms.TextBox txtNbPage;
        internal System.Windows.Forms.Label lbNbPages;
        private System.Windows.Forms.ComboBox dropDownAuteur;
        internal System.Windows.Forms.TextBox txtDateEdition;
        internal System.Windows.Forms.Label lbDateEdition;
        internal System.Windows.Forms.TextBox txtISBN;
        internal System.Windows.Forms.Label lbISBN;
        internal System.Windows.Forms.Label lbAuteur;
        internal System.Windows.Forms.TextBox txtTitre;
        internal System.Windows.Forms.Label lbTitre;
        internal System.Windows.Forms.Button btnAddLivre;
        internal System.Windows.Forms.Button btnDelLivre;
        private System.Windows.Forms.ColumnHeader colHeaderLivresEditeur;
        private System.Windows.Forms.ComboBox dropDownEditeur;
        internal System.Windows.Forms.Label lbEditeur;
        private System.Windows.Forms.Label lbListeAdherent;
        internal System.Windows.Forms.Label lbListeEditeurs;
        internal System.Windows.Forms.Label lbListeAuteurs;
        private System.Windows.Forms.Label lbListeLivres;
        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem toolStripAdministration;
        private System.Windows.Forms.ToolStripMenuItem toolStripGestionEmprunts;
        private System.Windows.Forms.TabControl tabControlGestion;
        private System.Windows.Forms.TabPage tabPageRetour;
        private System.Windows.Forms.Label lbListeExemplaires;
        internal System.Windows.Forms.ListView grdExemplaire;
        internal System.Windows.Forms.Button btnEditExemplaire;
        internal System.Windows.Forms.GroupBox grpDetailsExemplaire;
        private System.Windows.Forms.ComboBox dropDownLivre;
        internal System.Windows.Forms.Label lbEtat2;
        internal System.Windows.Forms.TextBox txtDateAcquis;
        internal System.Windows.Forms.Label lbDateAcquis;
        internal System.Windows.Forms.Button btnAddExemplaire;
        internal System.Windows.Forms.Button btnDelExemplaire;
        private System.Windows.Forms.ComboBox dropDownEtat;
        internal System.Windows.Forms.Label lbEtat;
        private System.Windows.Forms.ColumnHeader colHeaderLivreQte;
        private System.Windows.Forms.DateTimePicker dateTPickerExemplaire;
        private System.Windows.Forms.DateTimePicker dateTPickerAdherent;
        private System.Windows.Forms.DateTimePicker dateTPickerLivre;
        private System.Windows.Forms.GroupBox grpDetailsEmprunt;
        private System.Windows.Forms.Label lbEmprunt;
        private System.Windows.Forms.ColumnHeader colHeaderEmprNom;
        private System.Windows.Forms.ColumnHeader colHeaderEmprPrenom;
        private System.Windows.Forms.ColumnHeader colHeaderEmprQte;
        internal System.Windows.Forms.Button btnRestituer;
        private System.Windows.Forms.ColumnHeader colHeaderExemplaireID;
        private System.Windows.Forms.ColumnHeader colHeaderExemplaireLivreID;
        private System.Windows.Forms.ColumnHeader colHeaderExemplaireTitre;
        private System.Windows.Forms.ColumnHeader colHeaderExemplaireEtat;
        private System.Windows.Forms.ColumnHeader colHeaderExemplaireDateAcquis;
        private System.Windows.Forms.TabPage tabPageEmprunt;
        private System.Windows.Forms.ColumnHeader ccolHeaderEmprID;
        internal System.Windows.Forms.ListView grdEmprunt;
        internal System.Windows.Forms.ListView grdListEmprunt;
        private System.Windows.Forms.ColumnHeader colHeaderEmprId;
        private System.Windows.Forms.ColumnHeader colHeaderEmprISBN;
        private System.Windows.Forms.ColumnHeader colHeaderEmprDEmpr;
        private System.Windows.Forms.ColumnHeader colHeaderEmprDRetour;
        internal System.Windows.Forms.Button btnPret;
        private System.Windows.Forms.DateTimePicker dateTPickerPret;
        internal System.Windows.Forms.TextBox txtDatePret;
        internal System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox dropDownLivrePret;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox dropDownAdherent;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox grpDetailsPretExemplaire;
        internal System.Windows.Forms.ListView grdListExemplaires;
        private System.Windows.Forms.ColumnHeader colHeaderPretID;
        private System.Windows.Forms.ColumnHeader colHeaderPretEtat;
        private System.Windows.Forms.ColumnHeader colHeaderPretDRetour;
        private System.Windows.Forms.ComboBox dropDownExemplaire;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.TextBox txtExemplaire;
        internal System.Windows.Forms.TextBox txtLivre;
        internal System.Windows.Forms.TextBox txtAdherent;
        internal System.Windows.Forms.Label lbidEPret;
        internal System.Windows.Forms.Label lbidLPret;
        internal System.Windows.Forms.Label lbidAPret;
        internal System.Windows.Forms.Label label5;

    }
}

