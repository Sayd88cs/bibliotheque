﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bibliotheque
{
    class EditeurControlleur
    {
        private Editeur modelEditeur = ModelReq.LoadModel<Editeur>();

        public void AjouterEditeur(Editeur unEditeur)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            foreach (PropertyInfo p in unEditeur.GetType().GetProperties())
            {
                if (p.GetValue(unEditeur) != null)
                {
                    data.Add(p.Name, p.GetValue(unEditeur).ToString());
                }
            }
            modelEditeur.BDD_Save(data);
        }
    }
}
