﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bibliotheque
{
    class ExemplaireControlleur
    {
        private Exemplaire modelExemplaire = ModelReq.LoadModel<Exemplaire>();

        /// <summary>
        /// Methode permettant d'ajouter un exemplaire d'un livre
        /// </summary>
        /// <param name="unExemplaire"></param>
        public void AjouterExemplaire(Exemplaire unExemplaire)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            foreach (PropertyInfo p in unExemplaire.GetType().GetProperties())
            {
                if (p.GetValue(unExemplaire) != null)
                {
                    data.Add(p.Name, p.GetValue(unExemplaire).ToString());
                }
            }
            data["dateAcquisition"] = unExemplaire.dateAcquisition.ToString("yyyy-MM-dd");
            data["id_Livre"] = unExemplaire.id_Livre.id;
            modelExemplaire.BDD_Save(data);
        }


        /// <summary>
        /// Retourne le nombre d'exemplaire du livre envoyé en paramètre
        /// </summary>
        /// <param name="idLivre"></param>
        /// <returns></returns>
        public string CountExemplaire(string idLivre)
        {
            string quantite;
            string condition;
            condition = "id_Livre = " + idLivre;
            quantite = modelExemplaire.BDD_Read<Exemplaire>(default(int), condition).Count().ToString();
            return quantite;
        }
    }
}
