﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bibliotheque
{
    class AdherentControlleur
    {
        private Adherent modelAdherent = ModelReq.LoadModel<Adherent>();

        public void AjouterAdherent(Adherent unAdherent)
        {

            Dictionary<string, string> data = new Dictionary<string, string>();
            foreach (PropertyInfo p in unAdherent.GetType().GetProperties())
            {
                if(p.GetValue(unAdherent) != null)
                {
                    data.Add(p.Name, p.GetValue(unAdherent).ToString());
                } 
            }
            data["dateNaissance"] = unAdherent.dateNaissance.ToString("yyyy-MM-dd");
            modelAdherent.BDD_Save(data);
        }
    }
}
