﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bibliotheque
{
    class LivreControlleur
    {
        private Livre modelLivre = ModelReq.LoadModel<Livre>();

        /// <summary>
        /// Methode permettant d'ajouter un livre au catalogue
        /// </summary>
        /// <param name="unLivre"></param>
        public void AjouterLivre(Livre unLivre)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            foreach (PropertyInfo p in unLivre.GetType().GetProperties())
            {
                if (p.GetValue(unLivre) != null)
                {
                    data.Add(p.Name, p.GetValue(unLivre).ToString());
                }
            }
            data["dateEdition"] = unLivre.dateEdition.ToString("yyyy-MM-dd");
            data["id_Editeur"] = unLivre.id_Editeur.id;
            data["id_Auteur"] = unLivre.id_Auteur.id;
            
            modelLivre.BDD_Save(data);
        }

        /// <summary>
        /// Permet de mettre à jour un livre existant en base de donnée
        /// </summary>
        /// <param name="unLivre"></param>
        /// <param name="id"></param>
        public void MiseAjourLivre(Livre unLivre, int id)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("id", id.ToString());
            foreach (PropertyInfo p in unLivre.GetType().GetProperties())
            {
                data.Add(p.Name, p.GetValue(unLivre).ToString());
            }
            modelLivre.BDD_Save(data);
        }

        public Livre ObtenirLivre(int id)
        {
            Livre unLivre = new Livre();
            unLivre = modelLivre.BDD_Read<Livre>(id)[0];
            return unLivre;
        }
    }
}
