﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bibliotheque
{
    class AuteurControlleur
    {
        private Auteur modelAuteur = ModelReq.LoadModel<Auteur>();

        public void AjouterAuteur(Auteur unAuteur)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            foreach (PropertyInfo p in unAuteur.GetType().GetProperties())
            {
                if (p.GetValue(unAuteur) != null)
                {
                    data.Add(p.Name, p.GetValue(unAuteur).ToString());
                }
            }
            modelAuteur.BDD_Save(data);
        }
    }
}
