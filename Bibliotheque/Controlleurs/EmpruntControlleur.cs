﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bibliotheque
{
    class EmpruntControlleur
    {
        private Emprunt modelEmprunt = ModelReq.LoadModel<Emprunt>();
        private Livre modelLivre = ModelReq.LoadModel<Livre>();
        public List<Livre> lesLivres { get; private set; }
        
        /// <summary>
        /// Retourne le nombre d'exemplaire du livre envoyé en paramètre
        /// </summary>
        /// <param name="idLivre"></param>
        /// <returns></returns>
        public string CountEmprunt(string idAdherent)
        {
            string quantite;
            string condition;
            condition = "id_Adherent = " + idAdherent;
            quantite = modelEmprunt.BDD_Read<Emprunt>(default(int),condition).Count().ToString();
            return quantite;
        }

        //private Adherent modelAdherent = ModelReq.LoadModel<Adherent>();

        public bool AjouterEmprunt(Emprunt unEmprunt)
        {
            bool reussi = false;
            Emprunt testEmprunt = modelEmprunt.BDD_Read<Emprunt>(default(int),"(id_Adherent = "+ unEmprunt.id_Adherent.id +" AND id_Livre = "+unEmprunt.id_Livre.id+" )")[0];
            if (testEmprunt == null)
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                foreach (PropertyInfo p in unEmprunt.GetType().GetProperties())
                {
                    if (p.GetValue(unEmprunt) != null)
                    {
                        data.Add(p.Name, p.GetValue(unEmprunt).ToString());
                    }
                }
                data["dateEmprunt"] = unEmprunt.dateEmprunt.ToString("yyyy-MM-dd");
                data["dateRetour"] = unEmprunt.dateRetour.ToString("yyyy-MM-dd");
                data["id_Adherent"] = unEmprunt.id_Adherent.id;
                data["id_Exemplaire"] = unEmprunt.id_Exemplaire.id;
                data["id_Livre"] = unEmprunt.id_Livre.id;
                modelEmprunt.BDD_Save(data);
                reussi = true;
            }
            return reussi;
        }

        /// <summary>
        /// Retourne la lsite des emprunt de l'Adhérent
        /// </summary>
        /// <param name="idAdherent"></param>
        /// <returns></returns>
        public List<Livre> ListeEmpruntAdherent(string idAdherent)
        {
            lesLivres = new List<Livre>();
            string condition = "id IN(SELECT id_Livre From emprunt where id_Adherent = "+ idAdherent +")";
            lesLivres = modelLivre.BDD_Read<Livre>(default(int),condition);
            return lesLivres; 
        }
    }
}
