﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Bibliotheque
{
    class Exemplaire : ModelReq
    {
        public string id { get; private set; }
        public Livre id_Livre { get; private set; }
        public string etat { get; private set; }
        public DateTime dateAcquisition { get; private set; }


        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public Exemplaire()
        {
            this.table = "Exemplaire";
        }

        /// <summary>
        /// Constructeur utilisable lors de la récupération dans les formulaires
        /// </summary>
        /// <param name="unLivre"></param>
        /// <param name="etat"></param>
        /// <param name="dateAcquisition"></param>
        public Exemplaire(int idLivre, string etat, DateTime dateAcquisition, string id = null)
        {
            this.table = "Exemplaire";
            this.id = (id != null) ? id : null;
            this.id_Livre = new Livre();
            this.id_Livre = id_Livre.BDD_Read<Livre>(idLivre)[0];
            this.etat = etat;
            this.dateAcquisition = dateAcquisition;
        }

        /// <summary>
        /// Constructeur utilisé lors de la récupération via la methode BDD_Read
        /// </summary>
        /// <param name="args"></param>
        public Exemplaire(List<string> args)
        {
            this.table = "Exemplaire";
            this.id = args[0];

            // On utilise l'argument id pour trouver le livre
            int id = int.Parse(args[1]);
            this.id_Livre = new Livre();
            this.id_Livre = id_Livre.BDD_Read<Livre>(id)[0];
            this.etat = args[2];
            this.dateAcquisition = Convert.ToDateTime(args[3]);
        }
    }
}
