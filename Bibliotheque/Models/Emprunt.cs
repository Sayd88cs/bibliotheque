﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Bibliotheque
{
    class Emprunt : ModelReq
    {
        public Livre id_Livre { get; private set; }
        public Exemplaire id_Exemplaire { get; private set; }
        public Adherent id_Adherent { get; private set; }
        public DateTime dateEmprunt { get; private set; }
        public DateTime dateRetour { get; private set; }

        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public Emprunt()
        {
            this.table = "Emprunt";
        }

        /// <summary>
        /// Constructeur utilisable lors de la récupération dans les formulaires
        /// </summary>
        /// <param name="unLivre"></param>
        /// <param name="unExemplaire"></param>
        /// <param name="unAdherent"></param>
        /// <param name="dateEmprunt"></param>
        /// <param name="dateRetour"></param>
        public Emprunt(int idLivre, int idExemplaire, int idAdherent, DateTime dateEmprunt, DateTime dateRetour)
        {
            this.table = "Emprunt";
            this.id_Livre = new Livre();
            this.id_Livre = id_Livre.BDD_Read<Livre>(idLivre)[0];
            this.id_Exemplaire = new Exemplaire();
            this.id_Exemplaire = id_Exemplaire.BDD_Read<Exemplaire>(idExemplaire)[0];
            this.id_Adherent = new Adherent();
            this.id_Adherent = id_Adherent.BDD_Read<Adherent>(idAdherent)[0];
            this.dateEmprunt = dateEmprunt;
            this.dateRetour = dateRetour;
        }

        /// <summary>
        /// Constructeur utilisé lors de la récupération via la methode BDD_Read
        /// Le paramètre args est ordonnée de manière identique entre la BDD et la class
        /// </summary>
        /// <param name="args"></param>
        public Emprunt(List<string> args)
        {
            int id;
            this.table = "Emprunt";

            // On utilise l'argument id pour trouver le livre concerné
            id = int.Parse(args[0]);
            this.id_Livre = new Livre();
            this.id_Livre = id_Livre.BDD_Read<Livre>(id)[0];

            // On utilise l'argument id pour trouver l'exemplaire concerné
            id = int.Parse(args[1]);
            this.id_Exemplaire = new Exemplaire();
            this.id_Exemplaire = id_Exemplaire.BDD_Read<Exemplaire>(id)[0];

            // On utilise l'argument id pour trouver l'adhérent concerné
            id = int.Parse(args[2]);
            this.id_Adherent = new Adherent();
            this.id_Adherent = id_Adherent.BDD_Read<Adherent>(id)[0];

            this.dateEmprunt = Convert.ToDateTime(args[3]);
            this.dateRetour = Convert.ToDateTime(args[4]);
        }
    }
}
