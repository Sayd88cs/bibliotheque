﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Bibliotheque
{
    public class Adherent : ModelReq
    {
        public string id { get; private set; }
        public string nom { get; private set; }
        public string prenom { get; private set; }
        public string sexe { get; private set; }
        public DateTime dateNaissance { get; private set; }
        public string adresse { get; private set; }
        public string ville { get; private set; }
        public string codePostal { get; private set; }


        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public Adherent()
        {
            this.table = "Adherent";
        }

        /// <summary>
        /// Constructeur utilisable lors de la récupération dans les formulaires
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="prenom"></param>
        /// <param name="dateNaissance"></param>
        /// <param name="adresse"></param>
        /// <param name="ville"></param>
        /// <param name="codePostale"></param>
        public Adherent(string nom, string prenom,string sexe, DateTime dateNaissance, string adresse, string ville, string codePostal,string id = null)
        {
            this.id = (id != null) ? id : null;
            this.table = "Adherent";
            this.nom = nom;
            this.sexe = sexe;
            this.prenom = prenom;
            this.dateNaissance = dateNaissance;
            this.adresse = adresse;
            this.ville = ville;
            this.codePostal = codePostal;
        }

        /// <summary>
        /// Constructeur utilisé lors de la récupération via la methode BDD_Read
        /// </summary>
        /// <param name="args"></param>
        public Adherent(List<string> args)
        {
            this.table = "Adherent";
            this.id = args[0];
            this.nom = args[1];
            this.prenom = args[2];
            this.sexe = args[3];
            this.dateNaissance = DateTime.Parse(args[4]);
            this.adresse = args[5];
            this.ville = args[6];
            this.codePostal = args[7];
        }
    }
}
