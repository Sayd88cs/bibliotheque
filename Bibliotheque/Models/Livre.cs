﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Bibliotheque
{
    class Livre : ModelReq
    {
        public string id { get; private set; }
        public string titre { get; private set; }
        public string ISBN { get; private set; }
        public Editeur id_Editeur { get; private set; }
        public Auteur id_Auteur { get; private set; }
        public DateTime dateEdition { get; private set; }
        public int nbPages { get; private set; }

        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public Livre()
        {
            this.table = "Livre";
        }

        /// <summary>
        /// Constructeur utilisable lors de la récupération dans les formulaires
        /// </summary>
        /// <param name="titre"></param>
        /// <param name="ISBN"></param>
        /// <param name="idEditeur"></param>
        /// <param name="dateEdition"></param>
        /// <param name="nbPages"></param>
        public Livre(string titre, string ISBN, int idEditeur,int idAuteur, DateTime dateEdition, int nbPages,string id = null)
        {
            this.table = "Livre";
            this.id = (id != null) ? id : null;
            this.titre = titre;
            this.ISBN = ISBN;
            this.id_Editeur = new Editeur();
            this.id_Editeur = id_Editeur.BDD_Read<Editeur>(idEditeur)[0];
            this.id_Auteur = new Auteur();
            this.id_Auteur = id_Auteur.BDD_Read<Auteur>(idAuteur)[0];
            this.dateEdition = dateEdition;
            this.nbPages = nbPages;
        }

        /// <summary>
        /// Constructeur utilisé lors de la récupération via la methode BDD_Read
        /// </summary>
        /// <param name="args"></param>
        public Livre(List<string> args)
        {
            this.table = "Livre";
            this.id = args[0];
            this.titre = args[1];
            this.ISBN = args[2];

            // On utilise l'argument id pour trouver l'éditeur
            int idEditeur = int.Parse(args[3]);
            this.id_Editeur = new Editeur();
            this.id_Editeur = id_Editeur.BDD_Read<Editeur>(idEditeur)[0];

            //On utilise l'argument id pour trouver l'auteur
            int idAuteur = int.Parse(args[4]);
            this.id_Auteur = new Auteur();
            this.id_Auteur = id_Auteur.BDD_Read<Auteur>(idAuteur)[0];
            this.dateEdition = Convert.ToDateTime(args[5]);
            this.nbPages = int.Parse(args[6]);

        }
    }
}