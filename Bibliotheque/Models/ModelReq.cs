﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bibliotheque
{
    public abstract class ModelReq
    {
        private string connectionString;
        protected string table;

        //Constructeur initilisant l'accès à la BDD
        public ModelReq()
        {
            Initialize();
        }

        /// <summary>
        /// Charge les informations requise pour la connection à la BDD.
        /// </summary>
        private void Initialize()
        {
            string server = "127.0.0.1";
            string database = "aptus_info";
            string uid = "root";
            string password = "root";
            string convZeroDatetime = " convert zero datetime=True";
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";" + convZeroDatetime + ";";
        }

        /// <summary>
        /// Permet de charger un model de couche d'accès à la base de donnée
        /// </summary>
        /// <typeparam name="Model"></typeparam>
        /// <returns></returns>
        public static Model LoadModel<Model>()
        {
            return Activator.CreateInstance<Model>();
        }

        /// <summary>
        /// Lit les données en BDD et retourne sous forme de liste d'objets du type demandé lors de l'appel
        /// </summary>
        /// <typeparam name="Model"></typeparam>
        /// <param name="id"></param>
        /// <param name="condition"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public List<Model> BDD_Read<Model>(int id = default(int), string condition = null, List<string> fields = null, string groupBy = null)
        {
            Model unModel = default(Model);
            List<Model> lesModel = new List<Model>();
            List<string> proprietes = new List<string>();
            Dictionary<string, string> donnees = new Dictionary<string, string>();
            //S'il n'y'a aucun champs envoyé alors * sinon on récupère les champs avec un string.Join
            string field = (fields != null) ? string.Join(" , ", fields.ToArray()) : " * ";
            string query = "SELECT " + field + " FROM " + table;
            query += (id != default(int)) ? " WHERE id = @id " : "";
            query += (condition != null) ? ((id != default(int)) ? " AND " : " WHERE ") + condition : "";
            query += (groupBy != null) ? groupBy : "";
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.Parameters.AddWithValue("@id", id);
                    //Create a data reader and Execute the command
                    using (MySqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        DataTable dataBDD = new DataTable();
                        dataBDD.Load(dataReader);
                        if (dataBDD.Rows.Count >= 1)
                        {
                            foreach (DataRow ligne in dataBDD.Rows)
                            {
                                foreach (DataColumn col in dataBDD.Columns)
                                {
                                    proprietes.Add(ligne[col].ToString());
                                }
                                unModel = (Model)Activator.CreateInstance(typeof(Model), proprietes);
                                lesModel.Add(unModel);
                                proprietes.Clear();
                            }
                        }
                        else
                        {
                            lesModel.Add(unModel);
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                throw new Exception("Erreur code :  BDD-L-ModelReq-L53 ... Impossible de récuperer les données");
            }
            return lesModel;
        }

        /// <summary>
        /// Permet de sauvegarder ou de mêtre à jour une ligne dans la base de donnée
        /// </summary>
        /// <param name="champs"></param>
        public void BDD_Save(Dictionary<string, string> champs, bool fakeID = false)
        {
            string query;
            //champs["dateNaissance"] = champs["dateNaissance"].Substring(0, 10);
            //Si c'est un update il faut donner un id alors UPDATE
            if (champs.ContainsKey("id") || fakeID != false)
            {
                query = "UPDATE " + table + " SET ";
                int i = -1;
                foreach (KeyValuePair<string, string> field in champs)
                {
                    query += (field.Key != "id") ? field.Key + " = @field_" + i + "," : "";
                    i++;
                }
                //On suprime la dernière virgule ajouté lors de la boucle foreach
                query = query.Remove(query.Length - 1);
                if (champs.ContainsKey("id"))
                {
                    query += " WHERE id = " + champs["id"];
                    champs.Remove("id");
                }
            }
            //Sinon on considère que c'est une nouvelle entré donc INSERT
            else
            {
                int i = 0;
                query = "INSERT INTO " + table + " (";
                foreach (string field in champs.Keys)
                {
                    query += field + ",";
                }
                query = query.Remove(query.Length - 1);
                query += ") ";
                query += " VALUES (";
                foreach (string field in champs.Values)
                {
                    query += "@field_" + i + ",";
                    i++;
                }
                query = query.Remove(query.Length - 1);
                query += ")";
            }
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    int i = 0;
                    foreach (KeyValuePair<string, string> uneLigne in champs)
                    {
                        cmd.Parameters.AddWithValue("@field_" + i, uneLigne.Value);
                        i++;
                    }
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch
            {
                throw new Exception("Erreur code : BDD-IU-ModelReq-L161 ... Insertion/mise à jour impossible ");
            }
        }

        /// <summary>
        /// Permet de suprimer une occurence en BDD selon l'id passé en paramètre
        /// </summary>
        /// <param name="id"></param>
        public void BDD_Delete(int id = default(int), string condition = null)
        {
            string query;
            query = "DELETE FROM " + table + " WHERE ";
            query += (id != default(int))?"id = @id" : condition;
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch
            {
                throw new Exception("Erreur code : BDD-S-ModelReq-L161 ... Supression impossible ");
            }

        }

    }
}
