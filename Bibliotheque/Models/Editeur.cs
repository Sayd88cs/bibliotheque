﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Bibliotheque
{
    class Editeur : ModelReq
    {
        public string id { get; private set; }
        public string nom { get; private set; }
        public string prenom { get; private set; }

        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public Editeur()
        {
            this.table = "Editeur";
        }

        /// <summary>
        /// Constructeur utilisable lors de la récupération dans les formulaires
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="prenom"></param>
        public Editeur(string nom, string prenom, string id = null)
        {

            this.id = (id != null) ? id : null;
            this.table = "Editeur";
            this.nom = nom;
            this.prenom = prenom;
        }

        /// <summary>
        /// Constructeur utilisé lors de la récupération via la methode BDD_Read
        /// </summary>
        /// <param name="args"></param>
        public Editeur(List<string> args)
        {
            this.table = "Editeur";
            this.id = args[0];
            this.nom = args[1];
            this.prenom = args[2];
        }
    }
}
