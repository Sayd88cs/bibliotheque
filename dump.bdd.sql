-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 19 Septembre 2014 à 22:27
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `aptus_info`
--
CREATE DATABASE IF NOT EXISTS `aptus_info` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `aptus_info`;

-- --------------------------------------------------------

--
-- Structure de la table `adherent`
--

CREATE TABLE IF NOT EXISTS `adherent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `sexe` char(1) NOT NULL,
  `dateNaissance` date NOT NULL,
  `adresse` varchar(30) DEFAULT NULL,
  `ville` varchar(30) DEFAULT NULL,
  `codePostal` char(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Contenu de la table `adherent`
--

INSERT INTO `adherent` (`id`, `nom`, `prenom`, `sexe`, `dateNaissance`, `adresse`, `ville`, `codePostal`) VALUES
(46, 'Marc', 'Dadel', 'M', '2005-01-01', '3 rue des Machin', 'Les Mureaux', '78130'),
(48, 'Dalor', 'Linda', 'F', '1985-12-01', '1 rue de la cha', 'Tours', '32000');

-- --------------------------------------------------------

--
-- Structure de la table `auteur`
--

CREATE TABLE IF NOT EXISTS `auteur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) DEFAULT NULL,
  `prenom` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `auteur`
--

INSERT INTO `auteur` (`id`, `nom`, `prenom`) VALUES
(10, 'Sperling', 'Sacha'),
(11, 'Marot', 'Jean');

-- --------------------------------------------------------

--
-- Structure de la table `editeur`
--

CREATE TABLE IF NOT EXISTS `editeur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) DEFAULT NULL,
  `prenom` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `editeur`
--

INSERT INTO `editeur` (`id`, `nom`, `prenom`) VALUES
(5, 'Famille', ' Brossollet'),
(8, 'Grupo', 'Planeta'),
(9, 'Famille', ' Brossollet'),
(11, 'La Martinière', 'Groupe'),
(12, 'dedeéé"c', 'Groupe');

-- --------------------------------------------------------

--
-- Structure de la table `emprunt`
--

CREATE TABLE IF NOT EXISTS `emprunt` (
  `id_Livre` int(11) NOT NULL,
  `id_Exemplaire` int(11) NOT NULL,
  `id_Adherent` int(11) NOT NULL,
  `dateEmprunt` date DEFAULT NULL,
  `dateRetour` date DEFAULT NULL,
  PRIMARY KEY (`id_Exemplaire`,`id_Livre`),
  KEY `emprunt_ibfk_2` (`id_Adherent`),
  KEY `emprunt_ibfk_3` (`id_Livre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `emprunt`
--

INSERT INTO `emprunt` (`id_Livre`, `id_Exemplaire`, `id_Adherent`, `dateEmprunt`, `dateRetour`) VALUES
(67, 64, 46, '2014-06-16', '2014-05-09');

-- --------------------------------------------------------

--
-- Structure de la table `exemplaire`
--

CREATE TABLE IF NOT EXISTS `exemplaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_Livre` int(11) NOT NULL,
  `etat` varchar(10) DEFAULT NULL,
  `dateAcquisition` date DEFAULT NULL,
  PRIMARY KEY (`id`,`id_Livre`),
  KEY `exemplaire_ibfk_1` (`id_Livre`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=69 ;

--
-- Contenu de la table `exemplaire`
--

INSERT INTO `exemplaire` (`id`, `id_Livre`, `etat`, `dateAcquisition`) VALUES
(59, 66, 'Correct', '2000-02-02'),
(60, 66, 'Mauvais', '2000-02-02'),
(61, 66, 'Correct', '2014-05-24'),
(62, 69, 'Correct', '2014-05-24'),
(63, 63, 'Correct', '2014-05-24'),
(64, 67, 'Correct', '2014-05-24'),
(65, 70, 'Correct', '2014-05-24'),
(66, 70, 'Correct', '2014-05-24'),
(67, 71, 'Moyen', '2014-05-24'),
(68, 68, 'Moyen', '2014-05-24');

-- --------------------------------------------------------

--
-- Structure de la table `livre`
--

CREATE TABLE IF NOT EXISTS `livre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(30) DEFAULT NULL,
  `ISBN` char(15) NOT NULL,
  `id_Editeur` int(11) NOT NULL,
  `id_Auteur` int(11) NOT NULL,
  `dateEdition` date NOT NULL,
  `nbPages` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `livre_ibfk_1` (`id_Editeur`),
  KEY `livre_ibfk_2` (`id_Auteur`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=72 ;

--
-- Contenu de la table `livre`
--

INSERT INTO `livre` (`id`, `titre`, `ISBN`, `id_Editeur`, `id_Auteur`, `dateEdition`, `nbPages`) VALUES
(63, 'Machin', 'DOIDEDDEOZZZZZZ', 8, 10, '2014-06-10', 200),
(66, 'Machin dedede', 'DOIDEDDEOZZZZZZ', 8, 10, '2014-06-10', 200),
(67, 'Machin', 'DOIDEDDEOZZZZZZ', 8, 10, '2014-06-10', 200),
(68, 'Machin dedede', 'DOIDEDDEOZZZZZZ', 8, 10, '2014-06-10', 200),
(69, 'Machin', 'DOIDEDDEOZZZZZZ', 8, 10, '2014-06-10', 200),
(70, 'Machin', 'DOIDEDDEOZZZZZZ', 8, 10, '2014-06-10', 200),
(71, 'Machin', 'DOIDEDDEOZZZZZZ', 8, 10, '2014-06-10', 200);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `emprunt`
--
ALTER TABLE `emprunt`
  ADD CONSTRAINT `emprunt_ibfk_1` FOREIGN KEY (`id_Exemplaire`) REFERENCES `exemplaire` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `emprunt_ibfk_2` FOREIGN KEY (`id_Adherent`) REFERENCES `adherent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `emprunt_ibfk_3` FOREIGN KEY (`id_Livre`) REFERENCES `livre` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `exemplaire`
--
ALTER TABLE `exemplaire`
  ADD CONSTRAINT `exemplaire_ibfk_1` FOREIGN KEY (`id_Livre`) REFERENCES `livre` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `livre`
--
ALTER TABLE `livre`
  ADD CONSTRAINT `livre_ibfk_1` FOREIGN KEY (`id_Editeur`) REFERENCES `editeur` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `livre_ibfk_2` FOREIGN KEY (`id_Auteur`) REFERENCES `auteur` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
